module gitlab.com/enem-data/schemas/go

go 1.16

require (
	github.com/golang/mock v1.6.0
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
)
