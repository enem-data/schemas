// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package metadata_v1

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// ExamBookStoreServiceClient is the client API for ExamBookStoreService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ExamBookStoreServiceClient interface {
	GetGlobal(ctx context.Context, in *ExamBookStoreServiceGetGlobalRequest, opts ...grpc.CallOption) (*ExamBookStoreServiceGetGlobalResponse, error)
	GetLocal(ctx context.Context, in *ExamBookStoreServiceGetLocalRequest, opts ...grpc.CallOption) (*ExamBookStoreServiceGetLocalResponse, error)
	Create(ctx context.Context, in *ExamBookStoreServiceCreateRequest, opts ...grpc.CallOption) (*ExamBookStoreServiceCreateResponse, error)
	Update(ctx context.Context, in *ExamBookStoreServiceUpdateRequest, opts ...grpc.CallOption) (*ExamBookStoreServiceUpdateResponse, error)
	Delete(ctx context.Context, in *ExamBookStoreServiceDeleteRequest, opts ...grpc.CallOption) (*ExamBookStoreServiceDeleteResponse, error)
}

type examBookStoreServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewExamBookStoreServiceClient(cc grpc.ClientConnInterface) ExamBookStoreServiceClient {
	return &examBookStoreServiceClient{cc}
}

func (c *examBookStoreServiceClient) GetGlobal(ctx context.Context, in *ExamBookStoreServiceGetGlobalRequest, opts ...grpc.CallOption) (*ExamBookStoreServiceGetGlobalResponse, error) {
	out := new(ExamBookStoreServiceGetGlobalResponse)
	err := c.cc.Invoke(ctx, "/enem.metadata.v1.ExamBookStoreService/GetGlobal", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *examBookStoreServiceClient) GetLocal(ctx context.Context, in *ExamBookStoreServiceGetLocalRequest, opts ...grpc.CallOption) (*ExamBookStoreServiceGetLocalResponse, error) {
	out := new(ExamBookStoreServiceGetLocalResponse)
	err := c.cc.Invoke(ctx, "/enem.metadata.v1.ExamBookStoreService/GetLocal", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *examBookStoreServiceClient) Create(ctx context.Context, in *ExamBookStoreServiceCreateRequest, opts ...grpc.CallOption) (*ExamBookStoreServiceCreateResponse, error) {
	out := new(ExamBookStoreServiceCreateResponse)
	err := c.cc.Invoke(ctx, "/enem.metadata.v1.ExamBookStoreService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *examBookStoreServiceClient) Update(ctx context.Context, in *ExamBookStoreServiceUpdateRequest, opts ...grpc.CallOption) (*ExamBookStoreServiceUpdateResponse, error) {
	out := new(ExamBookStoreServiceUpdateResponse)
	err := c.cc.Invoke(ctx, "/enem.metadata.v1.ExamBookStoreService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *examBookStoreServiceClient) Delete(ctx context.Context, in *ExamBookStoreServiceDeleteRequest, opts ...grpc.CallOption) (*ExamBookStoreServiceDeleteResponse, error) {
	out := new(ExamBookStoreServiceDeleteResponse)
	err := c.cc.Invoke(ctx, "/enem.metadata.v1.ExamBookStoreService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ExamBookStoreServiceServer is the server API for ExamBookStoreService service.
// All implementations must embed UnimplementedExamBookStoreServiceServer
// for forward compatibility
type ExamBookStoreServiceServer interface {
	GetGlobal(context.Context, *ExamBookStoreServiceGetGlobalRequest) (*ExamBookStoreServiceGetGlobalResponse, error)
	GetLocal(context.Context, *ExamBookStoreServiceGetLocalRequest) (*ExamBookStoreServiceGetLocalResponse, error)
	Create(context.Context, *ExamBookStoreServiceCreateRequest) (*ExamBookStoreServiceCreateResponse, error)
	Update(context.Context, *ExamBookStoreServiceUpdateRequest) (*ExamBookStoreServiceUpdateResponse, error)
	Delete(context.Context, *ExamBookStoreServiceDeleteRequest) (*ExamBookStoreServiceDeleteResponse, error)
	mustEmbedUnimplementedExamBookStoreServiceServer()
}

// UnimplementedExamBookStoreServiceServer must be embedded to have forward compatible implementations.
type UnimplementedExamBookStoreServiceServer struct {
}

func (UnimplementedExamBookStoreServiceServer) GetGlobal(context.Context, *ExamBookStoreServiceGetGlobalRequest) (*ExamBookStoreServiceGetGlobalResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetGlobal not implemented")
}
func (UnimplementedExamBookStoreServiceServer) GetLocal(context.Context, *ExamBookStoreServiceGetLocalRequest) (*ExamBookStoreServiceGetLocalResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetLocal not implemented")
}
func (UnimplementedExamBookStoreServiceServer) Create(context.Context, *ExamBookStoreServiceCreateRequest) (*ExamBookStoreServiceCreateResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedExamBookStoreServiceServer) Update(context.Context, *ExamBookStoreServiceUpdateRequest) (*ExamBookStoreServiceUpdateResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedExamBookStoreServiceServer) Delete(context.Context, *ExamBookStoreServiceDeleteRequest) (*ExamBookStoreServiceDeleteResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedExamBookStoreServiceServer) mustEmbedUnimplementedExamBookStoreServiceServer() {}

// UnsafeExamBookStoreServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ExamBookStoreServiceServer will
// result in compilation errors.
type UnsafeExamBookStoreServiceServer interface {
	mustEmbedUnimplementedExamBookStoreServiceServer()
}

func RegisterExamBookStoreServiceServer(s grpc.ServiceRegistrar, srv ExamBookStoreServiceServer) {
	s.RegisterService(&ExamBookStoreService_ServiceDesc, srv)
}

func _ExamBookStoreService_GetGlobal_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ExamBookStoreServiceGetGlobalRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ExamBookStoreServiceServer).GetGlobal(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/enem.metadata.v1.ExamBookStoreService/GetGlobal",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ExamBookStoreServiceServer).GetGlobal(ctx, req.(*ExamBookStoreServiceGetGlobalRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ExamBookStoreService_GetLocal_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ExamBookStoreServiceGetLocalRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ExamBookStoreServiceServer).GetLocal(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/enem.metadata.v1.ExamBookStoreService/GetLocal",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ExamBookStoreServiceServer).GetLocal(ctx, req.(*ExamBookStoreServiceGetLocalRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ExamBookStoreService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ExamBookStoreServiceCreateRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ExamBookStoreServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/enem.metadata.v1.ExamBookStoreService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ExamBookStoreServiceServer).Create(ctx, req.(*ExamBookStoreServiceCreateRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ExamBookStoreService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ExamBookStoreServiceUpdateRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ExamBookStoreServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/enem.metadata.v1.ExamBookStoreService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ExamBookStoreServiceServer).Update(ctx, req.(*ExamBookStoreServiceUpdateRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ExamBookStoreService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ExamBookStoreServiceDeleteRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ExamBookStoreServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/enem.metadata.v1.ExamBookStoreService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ExamBookStoreServiceServer).Delete(ctx, req.(*ExamBookStoreServiceDeleteRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// ExamBookStoreService_ServiceDesc is the grpc.ServiceDesc for ExamBookStoreService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var ExamBookStoreService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "enem.metadata.v1.ExamBookStoreService",
	HandlerType: (*ExamBookStoreServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetGlobal",
			Handler:    _ExamBookStoreService_GetGlobal_Handler,
		},
		{
			MethodName: "GetLocal",
			Handler:    _ExamBookStoreService_GetLocal_Handler,
		},
		{
			MethodName: "Create",
			Handler:    _ExamBookStoreService_Create_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _ExamBookStoreService_Update_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _ExamBookStoreService_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "enem/metadata/v1/exam_book_store.proto",
}
