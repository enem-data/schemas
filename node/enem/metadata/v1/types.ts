/* eslint-disable */
import Long from "long";
import _m0 from "protobufjs/minimal";

export const protobufPackage = "enem.metadata.v1";

export enum Subject {
  SUBJECT_UNSPECIFIED = 0,
  SUBJECT_LC = 1,
  SUBJECT_MT = 2,
  SUBJECT_CH = 3,
  SUBJECT_CN = 4,
  UNRECOGNIZED = -1,
}

export function subjectFromJSON(object: any): Subject {
  switch (object) {
    case 0:
    case "SUBJECT_UNSPECIFIED":
      return Subject.SUBJECT_UNSPECIFIED;
    case 1:
    case "SUBJECT_LC":
      return Subject.SUBJECT_LC;
    case 2:
    case "SUBJECT_MT":
      return Subject.SUBJECT_MT;
    case 3:
    case "SUBJECT_CH":
      return Subject.SUBJECT_CH;
    case 4:
    case "SUBJECT_CN":
      return Subject.SUBJECT_CN;
    case -1:
    case "UNRECOGNIZED":
    default:
      return Subject.UNRECOGNIZED;
  }
}

export function subjectToJSON(object: Subject): string {
  switch (object) {
    case Subject.SUBJECT_UNSPECIFIED:
      return "SUBJECT_UNSPECIFIED";
    case Subject.SUBJECT_LC:
      return "SUBJECT_LC";
    case Subject.SUBJECT_MT:
      return "SUBJECT_MT";
    case Subject.SUBJECT_CH:
      return "SUBJECT_CH";
    case Subject.SUBJECT_CN:
      return "SUBJECT_CN";
    default:
      return "UNKNOWN";
  }
}

export enum QuestionAnswer {
  QUESTION_ANSWER_UNSPECIFIED = 0,
  QUESTION_ANSWER_A = 1,
  QUESTION_ANSWER_B = 2,
  QUESTION_ANSWER_C = 3,
  QUESTION_ANSWER_D = 4,
  QUESTION_ANSWER_E = 5,
  UNRECOGNIZED = -1,
}

export function questionAnswerFromJSON(object: any): QuestionAnswer {
  switch (object) {
    case 0:
    case "QUESTION_ANSWER_UNSPECIFIED":
      return QuestionAnswer.QUESTION_ANSWER_UNSPECIFIED;
    case 1:
    case "QUESTION_ANSWER_A":
      return QuestionAnswer.QUESTION_ANSWER_A;
    case 2:
    case "QUESTION_ANSWER_B":
      return QuestionAnswer.QUESTION_ANSWER_B;
    case 3:
    case "QUESTION_ANSWER_C":
      return QuestionAnswer.QUESTION_ANSWER_C;
    case 4:
    case "QUESTION_ANSWER_D":
      return QuestionAnswer.QUESTION_ANSWER_D;
    case 5:
    case "QUESTION_ANSWER_E":
      return QuestionAnswer.QUESTION_ANSWER_E;
    case -1:
    case "UNRECOGNIZED":
    default:
      return QuestionAnswer.UNRECOGNIZED;
  }
}

export function questionAnswerToJSON(object: QuestionAnswer): string {
  switch (object) {
    case QuestionAnswer.QUESTION_ANSWER_UNSPECIFIED:
      return "QUESTION_ANSWER_UNSPECIFIED";
    case QuestionAnswer.QUESTION_ANSWER_A:
      return "QUESTION_ANSWER_A";
    case QuestionAnswer.QUESTION_ANSWER_B:
      return "QUESTION_ANSWER_B";
    case QuestionAnswer.QUESTION_ANSWER_C:
      return "QUESTION_ANSWER_C";
    case QuestionAnswer.QUESTION_ANSWER_D:
      return "QUESTION_ANSWER_D";
    case QuestionAnswer.QUESTION_ANSWER_E:
      return "QUESTION_ANSWER_E";
    default:
      return "UNKNOWN";
  }
}

export interface Competence {
  globalId: number;
  subject: Subject;
  localId: number;
  /** https://download.inep.gov.br/download/enem/matriz_referencia.pdf */
  description?: string | undefined;
}

export interface Skill {
  globalId: number;
  competenceId: number;
  localId: number;
  /** https://download.inep.gov.br/download/enem/matriz_referencia.pdf */
  description?: string | undefined;
}

export interface Question {
  globalId: number;
  year: number;
  subjectId: Subject;
  skillId?: number | undefined;
  answer: QuestionAnswer;
  /**
   * Indicates whether the answer of this question was successfully disputed by the community.
   * Disputed questions are not included in searches by competence or skill since they have failed their initial design.
   */
  disputed: boolean;
  /**
   * Indicates whether this questions was presented as part of the adapted ExamBook.
   * Questions from the adapted ExamBook are considered separately for statistics.
   */
  adaptedExam: boolean;
}

export interface ExamBook {
  globalId: number;
  year: number;
  /** The INEP ID for this particular ExamBook. These are unique within the same year. */
  localId: number;
  /**
   * The color of the ExamBook, used to identify the ordering of the questions by the participants.
   * Here the value is normalized to full lower case.
   */
  color: string;
  subject: Subject;
  /**
   * An ExamBook of SUBJECT_LC will always have 50 questions in this order:
   *  - English specific questions, numbered 1 - 5
   *  - Spanish specific questions, numbered 1 - 5
   *  - Native language questions, numbered 6 - 45
   *
   * ExamBooks of any other subject will always have exactly 45 questions.
   */
  questions: Question[];
}

const baseCompetence: object = { globalId: 0, subject: 0, localId: 0 };

export const Competence = {
  encode(
    message: Competence,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    if (message.subject !== 0) {
      writer.uint32(16).int32(message.subject);
    }
    if (message.localId !== 0) {
      writer.uint32(24).uint32(message.localId);
    }
    if (message.description !== undefined) {
      writer.uint32(34).string(message.description);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Competence {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCompetence } as Competence;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        case 2:
          message.subject = reader.int32() as any;
          break;
        case 3:
          message.localId = reader.uint32();
          break;
        case 4:
          message.description = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Competence {
    const message = { ...baseCompetence } as Competence;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    if (object.subject !== undefined && object.subject !== null) {
      message.subject = subjectFromJSON(object.subject);
    }
    if (object.localId !== undefined && object.localId !== null) {
      message.localId = Number(object.localId);
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = String(object.description);
    }
    return message;
  },

  toJSON(message: Competence): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    message.subject !== undefined &&
      (obj.subject = subjectToJSON(message.subject));
    message.localId !== undefined && (obj.localId = message.localId);
    message.description !== undefined &&
      (obj.description = message.description);
    return obj;
  },

  fromPartial(object: DeepPartial<Competence>): Competence {
    const message = { ...baseCompetence } as Competence;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    if (object.subject !== undefined && object.subject !== null) {
      message.subject = object.subject;
    }
    if (object.localId !== undefined && object.localId !== null) {
      message.localId = object.localId;
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = object.description;
    }
    return message;
  },
};

const baseSkill: object = { globalId: 0, competenceId: 0, localId: 0 };

export const Skill = {
  encode(message: Skill, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    if (message.competenceId !== 0) {
      writer.uint32(16).uint32(message.competenceId);
    }
    if (message.localId !== 0) {
      writer.uint32(24).uint32(message.localId);
    }
    if (message.description !== undefined) {
      writer.uint32(34).string(message.description);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Skill {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSkill } as Skill;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        case 2:
          message.competenceId = reader.uint32();
          break;
        case 3:
          message.localId = reader.uint32();
          break;
        case 4:
          message.description = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Skill {
    const message = { ...baseSkill } as Skill;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    if (object.competenceId !== undefined && object.competenceId !== null) {
      message.competenceId = Number(object.competenceId);
    }
    if (object.localId !== undefined && object.localId !== null) {
      message.localId = Number(object.localId);
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = String(object.description);
    }
    return message;
  },

  toJSON(message: Skill): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    message.competenceId !== undefined &&
      (obj.competenceId = message.competenceId);
    message.localId !== undefined && (obj.localId = message.localId);
    message.description !== undefined &&
      (obj.description = message.description);
    return obj;
  },

  fromPartial(object: DeepPartial<Skill>): Skill {
    const message = { ...baseSkill } as Skill;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    if (object.competenceId !== undefined && object.competenceId !== null) {
      message.competenceId = object.competenceId;
    }
    if (object.localId !== undefined && object.localId !== null) {
      message.localId = object.localId;
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = object.description;
    }
    return message;
  },
};

const baseQuestion: object = {
  globalId: 0,
  year: 0,
  subjectId: 0,
  answer: 0,
  disputed: false,
  adaptedExam: false,
};

export const Question = {
  encode(
    message: Question,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    if (message.year !== 0) {
      writer.uint32(16).uint32(message.year);
    }
    if (message.subjectId !== 0) {
      writer.uint32(24).int32(message.subjectId);
    }
    if (message.skillId !== undefined) {
      writer.uint32(32).uint32(message.skillId);
    }
    if (message.answer !== 0) {
      writer.uint32(40).int32(message.answer);
    }
    if (message.disputed === true) {
      writer.uint32(48).bool(message.disputed);
    }
    if (message.adaptedExam === true) {
      writer.uint32(56).bool(message.adaptedExam);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Question {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseQuestion } as Question;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        case 2:
          message.year = reader.uint32();
          break;
        case 3:
          message.subjectId = reader.int32() as any;
          break;
        case 4:
          message.skillId = reader.uint32();
          break;
        case 5:
          message.answer = reader.int32() as any;
          break;
        case 6:
          message.disputed = reader.bool();
          break;
        case 7:
          message.adaptedExam = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Question {
    const message = { ...baseQuestion } as Question;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    if (object.year !== undefined && object.year !== null) {
      message.year = Number(object.year);
    }
    if (object.subjectId !== undefined && object.subjectId !== null) {
      message.subjectId = subjectFromJSON(object.subjectId);
    }
    if (object.skillId !== undefined && object.skillId !== null) {
      message.skillId = Number(object.skillId);
    }
    if (object.answer !== undefined && object.answer !== null) {
      message.answer = questionAnswerFromJSON(object.answer);
    }
    if (object.disputed !== undefined && object.disputed !== null) {
      message.disputed = Boolean(object.disputed);
    }
    if (object.adaptedExam !== undefined && object.adaptedExam !== null) {
      message.adaptedExam = Boolean(object.adaptedExam);
    }
    return message;
  },

  toJSON(message: Question): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    message.year !== undefined && (obj.year = message.year);
    message.subjectId !== undefined &&
      (obj.subjectId = subjectToJSON(message.subjectId));
    message.skillId !== undefined && (obj.skillId = message.skillId);
    message.answer !== undefined &&
      (obj.answer = questionAnswerToJSON(message.answer));
    message.disputed !== undefined && (obj.disputed = message.disputed);
    message.adaptedExam !== undefined &&
      (obj.adaptedExam = message.adaptedExam);
    return obj;
  },

  fromPartial(object: DeepPartial<Question>): Question {
    const message = { ...baseQuestion } as Question;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    if (object.year !== undefined && object.year !== null) {
      message.year = object.year;
    }
    if (object.subjectId !== undefined && object.subjectId !== null) {
      message.subjectId = object.subjectId;
    }
    if (object.skillId !== undefined && object.skillId !== null) {
      message.skillId = object.skillId;
    }
    if (object.answer !== undefined && object.answer !== null) {
      message.answer = object.answer;
    }
    if (object.disputed !== undefined && object.disputed !== null) {
      message.disputed = object.disputed;
    }
    if (object.adaptedExam !== undefined && object.adaptedExam !== null) {
      message.adaptedExam = object.adaptedExam;
    }
    return message;
  },
};

const baseExamBook: object = {
  globalId: 0,
  year: 0,
  localId: 0,
  color: "",
  subject: 0,
};

export const ExamBook = {
  encode(
    message: ExamBook,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    if (message.year !== 0) {
      writer.uint32(16).uint32(message.year);
    }
    if (message.localId !== 0) {
      writer.uint32(24).uint32(message.localId);
    }
    if (message.color !== "") {
      writer.uint32(34).string(message.color);
    }
    if (message.subject !== 0) {
      writer.uint32(40).int32(message.subject);
    }
    for (const v of message.questions) {
      Question.encode(v!, writer.uint32(50).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ExamBook {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseExamBook } as ExamBook;
    message.questions = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        case 2:
          message.year = reader.uint32();
          break;
        case 3:
          message.localId = reader.uint32();
          break;
        case 4:
          message.color = reader.string();
          break;
        case 5:
          message.subject = reader.int32() as any;
          break;
        case 6:
          message.questions.push(Question.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExamBook {
    const message = { ...baseExamBook } as ExamBook;
    message.questions = [];
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    if (object.year !== undefined && object.year !== null) {
      message.year = Number(object.year);
    }
    if (object.localId !== undefined && object.localId !== null) {
      message.localId = Number(object.localId);
    }
    if (object.color !== undefined && object.color !== null) {
      message.color = String(object.color);
    }
    if (object.subject !== undefined && object.subject !== null) {
      message.subject = subjectFromJSON(object.subject);
    }
    if (object.questions !== undefined && object.questions !== null) {
      for (const e of object.questions) {
        message.questions.push(Question.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: ExamBook): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    message.year !== undefined && (obj.year = message.year);
    message.localId !== undefined && (obj.localId = message.localId);
    message.color !== undefined && (obj.color = message.color);
    message.subject !== undefined &&
      (obj.subject = subjectToJSON(message.subject));
    if (message.questions) {
      obj.questions = message.questions.map((e) =>
        e ? Question.toJSON(e) : undefined
      );
    } else {
      obj.questions = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<ExamBook>): ExamBook {
    const message = { ...baseExamBook } as ExamBook;
    message.questions = [];
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    if (object.year !== undefined && object.year !== null) {
      message.year = object.year;
    }
    if (object.localId !== undefined && object.localId !== null) {
      message.localId = object.localId;
    }
    if (object.color !== undefined && object.color !== null) {
      message.color = object.color;
    }
    if (object.subject !== undefined && object.subject !== null) {
      message.subject = object.subject;
    }
    if (object.questions !== undefined && object.questions !== null) {
      for (const e of object.questions) {
        message.questions.push(Question.fromPartial(e));
      }
    }
    return message;
  },
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined
  | Long;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends { $case: string }
  ? { [K in keyof Omit<T, "$case">]?: DeepPartial<T[K]> } & {
      $case: T["$case"];
    }
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
