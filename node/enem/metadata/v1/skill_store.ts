/* eslint-disable */
import Long from "long";
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from "@grpc/grpc-js";
import _m0 from "protobufjs/minimal";
import { Skill } from "../../../enem/metadata/v1/types";

export const protobufPackage = "enem.metadata.v1";

export interface SkillStoreServiceGetGlobalRequest {
  globalId: number;
}

export interface SkillStoreServiceGetGlobalResponse {
  skill?: Skill | undefined;
}

export interface SkillStoreServiceGetLocalRequest {
  competenceId: number;
  localId: number;
}

export interface SkillStoreServiceGetLocalResponse {
  skill?: Skill | undefined;
}

export interface SkillStoreServiceCreateRequest {
  competenceId: number;
  description?: string | undefined;
}

export interface SkillStoreServiceCreateResponse {
  skill: Skill | undefined;
}

export interface SkillStoreServiceUpdateRequest {
  globalId: number;
  description?: string | undefined;
}

export interface SkillStoreServiceUpdateResponse {
  skill: Skill | undefined;
}

export interface SkillStoreServiceDeleteRequest {
  globalId: number;
}

export interface SkillStoreServiceDeleteResponse {
  skill: Skill | undefined;
}

const baseSkillStoreServiceGetGlobalRequest: object = { globalId: 0 };

export const SkillStoreServiceGetGlobalRequest = {
  encode(
    message: SkillStoreServiceGetGlobalRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SkillStoreServiceGetGlobalRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseSkillStoreServiceGetGlobalRequest,
    } as SkillStoreServiceGetGlobalRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SkillStoreServiceGetGlobalRequest {
    const message = {
      ...baseSkillStoreServiceGetGlobalRequest,
    } as SkillStoreServiceGetGlobalRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    return message;
  },

  toJSON(message: SkillStoreServiceGetGlobalRequest): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<SkillStoreServiceGetGlobalRequest>
  ): SkillStoreServiceGetGlobalRequest {
    const message = {
      ...baseSkillStoreServiceGetGlobalRequest,
    } as SkillStoreServiceGetGlobalRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    return message;
  },
};

const baseSkillStoreServiceGetGlobalResponse: object = {};

export const SkillStoreServiceGetGlobalResponse = {
  encode(
    message: SkillStoreServiceGetGlobalResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.skill !== undefined) {
      Skill.encode(message.skill, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SkillStoreServiceGetGlobalResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseSkillStoreServiceGetGlobalResponse,
    } as SkillStoreServiceGetGlobalResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.skill = Skill.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SkillStoreServiceGetGlobalResponse {
    const message = {
      ...baseSkillStoreServiceGetGlobalResponse,
    } as SkillStoreServiceGetGlobalResponse;
    if (object.skill !== undefined && object.skill !== null) {
      message.skill = Skill.fromJSON(object.skill);
    }
    return message;
  },

  toJSON(message: SkillStoreServiceGetGlobalResponse): unknown {
    const obj: any = {};
    message.skill !== undefined &&
      (obj.skill = message.skill ? Skill.toJSON(message.skill) : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<SkillStoreServiceGetGlobalResponse>
  ): SkillStoreServiceGetGlobalResponse {
    const message = {
      ...baseSkillStoreServiceGetGlobalResponse,
    } as SkillStoreServiceGetGlobalResponse;
    if (object.skill !== undefined && object.skill !== null) {
      message.skill = Skill.fromPartial(object.skill);
    }
    return message;
  },
};

const baseSkillStoreServiceGetLocalRequest: object = {
  competenceId: 0,
  localId: 0,
};

export const SkillStoreServiceGetLocalRequest = {
  encode(
    message: SkillStoreServiceGetLocalRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.competenceId !== 0) {
      writer.uint32(8).uint32(message.competenceId);
    }
    if (message.localId !== 0) {
      writer.uint32(16).uint32(message.localId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SkillStoreServiceGetLocalRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseSkillStoreServiceGetLocalRequest,
    } as SkillStoreServiceGetLocalRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.competenceId = reader.uint32();
          break;
        case 2:
          message.localId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SkillStoreServiceGetLocalRequest {
    const message = {
      ...baseSkillStoreServiceGetLocalRequest,
    } as SkillStoreServiceGetLocalRequest;
    if (object.competenceId !== undefined && object.competenceId !== null) {
      message.competenceId = Number(object.competenceId);
    }
    if (object.localId !== undefined && object.localId !== null) {
      message.localId = Number(object.localId);
    }
    return message;
  },

  toJSON(message: SkillStoreServiceGetLocalRequest): unknown {
    const obj: any = {};
    message.competenceId !== undefined &&
      (obj.competenceId = message.competenceId);
    message.localId !== undefined && (obj.localId = message.localId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<SkillStoreServiceGetLocalRequest>
  ): SkillStoreServiceGetLocalRequest {
    const message = {
      ...baseSkillStoreServiceGetLocalRequest,
    } as SkillStoreServiceGetLocalRequest;
    if (object.competenceId !== undefined && object.competenceId !== null) {
      message.competenceId = object.competenceId;
    }
    if (object.localId !== undefined && object.localId !== null) {
      message.localId = object.localId;
    }
    return message;
  },
};

const baseSkillStoreServiceGetLocalResponse: object = {};

export const SkillStoreServiceGetLocalResponse = {
  encode(
    message: SkillStoreServiceGetLocalResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.skill !== undefined) {
      Skill.encode(message.skill, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SkillStoreServiceGetLocalResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseSkillStoreServiceGetLocalResponse,
    } as SkillStoreServiceGetLocalResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.skill = Skill.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SkillStoreServiceGetLocalResponse {
    const message = {
      ...baseSkillStoreServiceGetLocalResponse,
    } as SkillStoreServiceGetLocalResponse;
    if (object.skill !== undefined && object.skill !== null) {
      message.skill = Skill.fromJSON(object.skill);
    }
    return message;
  },

  toJSON(message: SkillStoreServiceGetLocalResponse): unknown {
    const obj: any = {};
    message.skill !== undefined &&
      (obj.skill = message.skill ? Skill.toJSON(message.skill) : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<SkillStoreServiceGetLocalResponse>
  ): SkillStoreServiceGetLocalResponse {
    const message = {
      ...baseSkillStoreServiceGetLocalResponse,
    } as SkillStoreServiceGetLocalResponse;
    if (object.skill !== undefined && object.skill !== null) {
      message.skill = Skill.fromPartial(object.skill);
    }
    return message;
  },
};

const baseSkillStoreServiceCreateRequest: object = { competenceId: 0 };

export const SkillStoreServiceCreateRequest = {
  encode(
    message: SkillStoreServiceCreateRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.competenceId !== 0) {
      writer.uint32(8).uint32(message.competenceId);
    }
    if (message.description !== undefined) {
      writer.uint32(18).string(message.description);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SkillStoreServiceCreateRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseSkillStoreServiceCreateRequest,
    } as SkillStoreServiceCreateRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.competenceId = reader.uint32();
          break;
        case 2:
          message.description = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SkillStoreServiceCreateRequest {
    const message = {
      ...baseSkillStoreServiceCreateRequest,
    } as SkillStoreServiceCreateRequest;
    if (object.competenceId !== undefined && object.competenceId !== null) {
      message.competenceId = Number(object.competenceId);
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = String(object.description);
    }
    return message;
  },

  toJSON(message: SkillStoreServiceCreateRequest): unknown {
    const obj: any = {};
    message.competenceId !== undefined &&
      (obj.competenceId = message.competenceId);
    message.description !== undefined &&
      (obj.description = message.description);
    return obj;
  },

  fromPartial(
    object: DeepPartial<SkillStoreServiceCreateRequest>
  ): SkillStoreServiceCreateRequest {
    const message = {
      ...baseSkillStoreServiceCreateRequest,
    } as SkillStoreServiceCreateRequest;
    if (object.competenceId !== undefined && object.competenceId !== null) {
      message.competenceId = object.competenceId;
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = object.description;
    }
    return message;
  },
};

const baseSkillStoreServiceCreateResponse: object = {};

export const SkillStoreServiceCreateResponse = {
  encode(
    message: SkillStoreServiceCreateResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.skill !== undefined) {
      Skill.encode(message.skill, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SkillStoreServiceCreateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseSkillStoreServiceCreateResponse,
    } as SkillStoreServiceCreateResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.skill = Skill.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SkillStoreServiceCreateResponse {
    const message = {
      ...baseSkillStoreServiceCreateResponse,
    } as SkillStoreServiceCreateResponse;
    if (object.skill !== undefined && object.skill !== null) {
      message.skill = Skill.fromJSON(object.skill);
    }
    return message;
  },

  toJSON(message: SkillStoreServiceCreateResponse): unknown {
    const obj: any = {};
    message.skill !== undefined &&
      (obj.skill = message.skill ? Skill.toJSON(message.skill) : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<SkillStoreServiceCreateResponse>
  ): SkillStoreServiceCreateResponse {
    const message = {
      ...baseSkillStoreServiceCreateResponse,
    } as SkillStoreServiceCreateResponse;
    if (object.skill !== undefined && object.skill !== null) {
      message.skill = Skill.fromPartial(object.skill);
    }
    return message;
  },
};

const baseSkillStoreServiceUpdateRequest: object = { globalId: 0 };

export const SkillStoreServiceUpdateRequest = {
  encode(
    message: SkillStoreServiceUpdateRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    if (message.description !== undefined) {
      writer.uint32(18).string(message.description);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SkillStoreServiceUpdateRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseSkillStoreServiceUpdateRequest,
    } as SkillStoreServiceUpdateRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        case 2:
          message.description = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SkillStoreServiceUpdateRequest {
    const message = {
      ...baseSkillStoreServiceUpdateRequest,
    } as SkillStoreServiceUpdateRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = String(object.description);
    }
    return message;
  },

  toJSON(message: SkillStoreServiceUpdateRequest): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    message.description !== undefined &&
      (obj.description = message.description);
    return obj;
  },

  fromPartial(
    object: DeepPartial<SkillStoreServiceUpdateRequest>
  ): SkillStoreServiceUpdateRequest {
    const message = {
      ...baseSkillStoreServiceUpdateRequest,
    } as SkillStoreServiceUpdateRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = object.description;
    }
    return message;
  },
};

const baseSkillStoreServiceUpdateResponse: object = {};

export const SkillStoreServiceUpdateResponse = {
  encode(
    message: SkillStoreServiceUpdateResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.skill !== undefined) {
      Skill.encode(message.skill, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SkillStoreServiceUpdateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseSkillStoreServiceUpdateResponse,
    } as SkillStoreServiceUpdateResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.skill = Skill.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SkillStoreServiceUpdateResponse {
    const message = {
      ...baseSkillStoreServiceUpdateResponse,
    } as SkillStoreServiceUpdateResponse;
    if (object.skill !== undefined && object.skill !== null) {
      message.skill = Skill.fromJSON(object.skill);
    }
    return message;
  },

  toJSON(message: SkillStoreServiceUpdateResponse): unknown {
    const obj: any = {};
    message.skill !== undefined &&
      (obj.skill = message.skill ? Skill.toJSON(message.skill) : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<SkillStoreServiceUpdateResponse>
  ): SkillStoreServiceUpdateResponse {
    const message = {
      ...baseSkillStoreServiceUpdateResponse,
    } as SkillStoreServiceUpdateResponse;
    if (object.skill !== undefined && object.skill !== null) {
      message.skill = Skill.fromPartial(object.skill);
    }
    return message;
  },
};

const baseSkillStoreServiceDeleteRequest: object = { globalId: 0 };

export const SkillStoreServiceDeleteRequest = {
  encode(
    message: SkillStoreServiceDeleteRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SkillStoreServiceDeleteRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseSkillStoreServiceDeleteRequest,
    } as SkillStoreServiceDeleteRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SkillStoreServiceDeleteRequest {
    const message = {
      ...baseSkillStoreServiceDeleteRequest,
    } as SkillStoreServiceDeleteRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    return message;
  },

  toJSON(message: SkillStoreServiceDeleteRequest): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<SkillStoreServiceDeleteRequest>
  ): SkillStoreServiceDeleteRequest {
    const message = {
      ...baseSkillStoreServiceDeleteRequest,
    } as SkillStoreServiceDeleteRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    return message;
  },
};

const baseSkillStoreServiceDeleteResponse: object = {};

export const SkillStoreServiceDeleteResponse = {
  encode(
    message: SkillStoreServiceDeleteResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.skill !== undefined) {
      Skill.encode(message.skill, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SkillStoreServiceDeleteResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseSkillStoreServiceDeleteResponse,
    } as SkillStoreServiceDeleteResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.skill = Skill.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SkillStoreServiceDeleteResponse {
    const message = {
      ...baseSkillStoreServiceDeleteResponse,
    } as SkillStoreServiceDeleteResponse;
    if (object.skill !== undefined && object.skill !== null) {
      message.skill = Skill.fromJSON(object.skill);
    }
    return message;
  },

  toJSON(message: SkillStoreServiceDeleteResponse): unknown {
    const obj: any = {};
    message.skill !== undefined &&
      (obj.skill = message.skill ? Skill.toJSON(message.skill) : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<SkillStoreServiceDeleteResponse>
  ): SkillStoreServiceDeleteResponse {
    const message = {
      ...baseSkillStoreServiceDeleteResponse,
    } as SkillStoreServiceDeleteResponse;
    if (object.skill !== undefined && object.skill !== null) {
      message.skill = Skill.fromPartial(object.skill);
    }
    return message;
  },
};

export const SkillStoreServiceService = {
  getGlobal: {
    path: "/enem.metadata.v1.SkillStoreService/GetGlobal",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: SkillStoreServiceGetGlobalRequest) =>
      Buffer.from(SkillStoreServiceGetGlobalRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      SkillStoreServiceGetGlobalRequest.decode(value),
    responseSerialize: (value: SkillStoreServiceGetGlobalResponse) =>
      Buffer.from(SkillStoreServiceGetGlobalResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      SkillStoreServiceGetGlobalResponse.decode(value),
  },
  getLocal: {
    path: "/enem.metadata.v1.SkillStoreService/GetLocal",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: SkillStoreServiceGetLocalRequest) =>
      Buffer.from(SkillStoreServiceGetLocalRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      SkillStoreServiceGetLocalRequest.decode(value),
    responseSerialize: (value: SkillStoreServiceGetLocalResponse) =>
      Buffer.from(SkillStoreServiceGetLocalResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      SkillStoreServiceGetLocalResponse.decode(value),
  },
  create: {
    path: "/enem.metadata.v1.SkillStoreService/Create",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: SkillStoreServiceCreateRequest) =>
      Buffer.from(SkillStoreServiceCreateRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      SkillStoreServiceCreateRequest.decode(value),
    responseSerialize: (value: SkillStoreServiceCreateResponse) =>
      Buffer.from(SkillStoreServiceCreateResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      SkillStoreServiceCreateResponse.decode(value),
  },
  update: {
    path: "/enem.metadata.v1.SkillStoreService/Update",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: SkillStoreServiceUpdateRequest) =>
      Buffer.from(SkillStoreServiceUpdateRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      SkillStoreServiceUpdateRequest.decode(value),
    responseSerialize: (value: SkillStoreServiceUpdateResponse) =>
      Buffer.from(SkillStoreServiceUpdateResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      SkillStoreServiceUpdateResponse.decode(value),
  },
  delete: {
    path: "/enem.metadata.v1.SkillStoreService/Delete",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: SkillStoreServiceDeleteRequest) =>
      Buffer.from(SkillStoreServiceDeleteRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      SkillStoreServiceDeleteRequest.decode(value),
    responseSerialize: (value: SkillStoreServiceDeleteResponse) =>
      Buffer.from(SkillStoreServiceDeleteResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      SkillStoreServiceDeleteResponse.decode(value),
  },
} as const;

export interface SkillStoreServiceServer extends UntypedServiceImplementation {
  getGlobal: handleUnaryCall<
    SkillStoreServiceGetGlobalRequest,
    SkillStoreServiceGetGlobalResponse
  >;
  getLocal: handleUnaryCall<
    SkillStoreServiceGetLocalRequest,
    SkillStoreServiceGetLocalResponse
  >;
  create: handleUnaryCall<
    SkillStoreServiceCreateRequest,
    SkillStoreServiceCreateResponse
  >;
  update: handleUnaryCall<
    SkillStoreServiceUpdateRequest,
    SkillStoreServiceUpdateResponse
  >;
  delete: handleUnaryCall<
    SkillStoreServiceDeleteRequest,
    SkillStoreServiceDeleteResponse
  >;
}

export interface SkillStoreServiceClient extends Client {
  getGlobal(
    request: SkillStoreServiceGetGlobalRequest,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceGetGlobalResponse
    ) => void
  ): ClientUnaryCall;
  getGlobal(
    request: SkillStoreServiceGetGlobalRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceGetGlobalResponse
    ) => void
  ): ClientUnaryCall;
  getGlobal(
    request: SkillStoreServiceGetGlobalRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceGetGlobalResponse
    ) => void
  ): ClientUnaryCall;
  getLocal(
    request: SkillStoreServiceGetLocalRequest,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceGetLocalResponse
    ) => void
  ): ClientUnaryCall;
  getLocal(
    request: SkillStoreServiceGetLocalRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceGetLocalResponse
    ) => void
  ): ClientUnaryCall;
  getLocal(
    request: SkillStoreServiceGetLocalRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceGetLocalResponse
    ) => void
  ): ClientUnaryCall;
  create(
    request: SkillStoreServiceCreateRequest,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceCreateResponse
    ) => void
  ): ClientUnaryCall;
  create(
    request: SkillStoreServiceCreateRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceCreateResponse
    ) => void
  ): ClientUnaryCall;
  create(
    request: SkillStoreServiceCreateRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceCreateResponse
    ) => void
  ): ClientUnaryCall;
  update(
    request: SkillStoreServiceUpdateRequest,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceUpdateResponse
    ) => void
  ): ClientUnaryCall;
  update(
    request: SkillStoreServiceUpdateRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceUpdateResponse
    ) => void
  ): ClientUnaryCall;
  update(
    request: SkillStoreServiceUpdateRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceUpdateResponse
    ) => void
  ): ClientUnaryCall;
  delete(
    request: SkillStoreServiceDeleteRequest,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceDeleteResponse
    ) => void
  ): ClientUnaryCall;
  delete(
    request: SkillStoreServiceDeleteRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceDeleteResponse
    ) => void
  ): ClientUnaryCall;
  delete(
    request: SkillStoreServiceDeleteRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: SkillStoreServiceDeleteResponse
    ) => void
  ): ClientUnaryCall;
}

export const SkillStoreServiceClient = makeGenericClientConstructor(
  SkillStoreServiceService,
  "enem.metadata.v1.SkillStoreService"
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): SkillStoreServiceClient;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined
  | Long;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends { $case: string }
  ? { [K in keyof Omit<T, "$case">]?: DeepPartial<T[K]> } & {
      $case: T["$case"];
    }
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
