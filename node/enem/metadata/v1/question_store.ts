/* eslint-disable */
import Long from "long";
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from "@grpc/grpc-js";
import _m0 from "protobufjs/minimal";
import { Question } from "../../../enem/metadata/v1/types";

export const protobufPackage = "enem.metadata.v1";

export interface QuestionStoreServiceGetGlobalRequest {
  globalId: number;
}

export interface QuestionStoreServiceGetGlobalResponse {
  question?: Question | undefined;
}

export interface QuestionStoreServiceGetLocalResponse {
  question?: Question | undefined;
}

export interface QuestionStoreServiceSaveRequest {
  question: Question | undefined;
}

export interface QuestionStoreServiceSaveResponse {
  question: Question | undefined;
}

export interface QuestionStoreServiceDeleteRequest {
  globalId: number;
}

export interface QuestionStoreServiceDeleteResponse {
  question: Question | undefined;
}

const baseQuestionStoreServiceGetGlobalRequest: object = { globalId: 0 };

export const QuestionStoreServiceGetGlobalRequest = {
  encode(
    message: QuestionStoreServiceGetGlobalRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): QuestionStoreServiceGetGlobalRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQuestionStoreServiceGetGlobalRequest,
    } as QuestionStoreServiceGetGlobalRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QuestionStoreServiceGetGlobalRequest {
    const message = {
      ...baseQuestionStoreServiceGetGlobalRequest,
    } as QuestionStoreServiceGetGlobalRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    return message;
  },

  toJSON(message: QuestionStoreServiceGetGlobalRequest): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QuestionStoreServiceGetGlobalRequest>
  ): QuestionStoreServiceGetGlobalRequest {
    const message = {
      ...baseQuestionStoreServiceGetGlobalRequest,
    } as QuestionStoreServiceGetGlobalRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    return message;
  },
};

const baseQuestionStoreServiceGetGlobalResponse: object = {};

export const QuestionStoreServiceGetGlobalResponse = {
  encode(
    message: QuestionStoreServiceGetGlobalResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.question !== undefined) {
      Question.encode(message.question, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): QuestionStoreServiceGetGlobalResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQuestionStoreServiceGetGlobalResponse,
    } as QuestionStoreServiceGetGlobalResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.question = Question.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QuestionStoreServiceGetGlobalResponse {
    const message = {
      ...baseQuestionStoreServiceGetGlobalResponse,
    } as QuestionStoreServiceGetGlobalResponse;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromJSON(object.question);
    }
    return message;
  },

  toJSON(message: QuestionStoreServiceGetGlobalResponse): unknown {
    const obj: any = {};
    message.question !== undefined &&
      (obj.question = message.question
        ? Question.toJSON(message.question)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QuestionStoreServiceGetGlobalResponse>
  ): QuestionStoreServiceGetGlobalResponse {
    const message = {
      ...baseQuestionStoreServiceGetGlobalResponse,
    } as QuestionStoreServiceGetGlobalResponse;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromPartial(object.question);
    }
    return message;
  },
};

const baseQuestionStoreServiceGetLocalResponse: object = {};

export const QuestionStoreServiceGetLocalResponse = {
  encode(
    message: QuestionStoreServiceGetLocalResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.question !== undefined) {
      Question.encode(message.question, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): QuestionStoreServiceGetLocalResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQuestionStoreServiceGetLocalResponse,
    } as QuestionStoreServiceGetLocalResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.question = Question.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QuestionStoreServiceGetLocalResponse {
    const message = {
      ...baseQuestionStoreServiceGetLocalResponse,
    } as QuestionStoreServiceGetLocalResponse;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromJSON(object.question);
    }
    return message;
  },

  toJSON(message: QuestionStoreServiceGetLocalResponse): unknown {
    const obj: any = {};
    message.question !== undefined &&
      (obj.question = message.question
        ? Question.toJSON(message.question)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QuestionStoreServiceGetLocalResponse>
  ): QuestionStoreServiceGetLocalResponse {
    const message = {
      ...baseQuestionStoreServiceGetLocalResponse,
    } as QuestionStoreServiceGetLocalResponse;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromPartial(object.question);
    }
    return message;
  },
};

const baseQuestionStoreServiceSaveRequest: object = {};

export const QuestionStoreServiceSaveRequest = {
  encode(
    message: QuestionStoreServiceSaveRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.question !== undefined) {
      Question.encode(message.question, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): QuestionStoreServiceSaveRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQuestionStoreServiceSaveRequest,
    } as QuestionStoreServiceSaveRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.question = Question.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QuestionStoreServiceSaveRequest {
    const message = {
      ...baseQuestionStoreServiceSaveRequest,
    } as QuestionStoreServiceSaveRequest;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromJSON(object.question);
    }
    return message;
  },

  toJSON(message: QuestionStoreServiceSaveRequest): unknown {
    const obj: any = {};
    message.question !== undefined &&
      (obj.question = message.question
        ? Question.toJSON(message.question)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QuestionStoreServiceSaveRequest>
  ): QuestionStoreServiceSaveRequest {
    const message = {
      ...baseQuestionStoreServiceSaveRequest,
    } as QuestionStoreServiceSaveRequest;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromPartial(object.question);
    }
    return message;
  },
};

const baseQuestionStoreServiceSaveResponse: object = {};

export const QuestionStoreServiceSaveResponse = {
  encode(
    message: QuestionStoreServiceSaveResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.question !== undefined) {
      Question.encode(message.question, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): QuestionStoreServiceSaveResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQuestionStoreServiceSaveResponse,
    } as QuestionStoreServiceSaveResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.question = Question.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QuestionStoreServiceSaveResponse {
    const message = {
      ...baseQuestionStoreServiceSaveResponse,
    } as QuestionStoreServiceSaveResponse;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromJSON(object.question);
    }
    return message;
  },

  toJSON(message: QuestionStoreServiceSaveResponse): unknown {
    const obj: any = {};
    message.question !== undefined &&
      (obj.question = message.question
        ? Question.toJSON(message.question)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QuestionStoreServiceSaveResponse>
  ): QuestionStoreServiceSaveResponse {
    const message = {
      ...baseQuestionStoreServiceSaveResponse,
    } as QuestionStoreServiceSaveResponse;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromPartial(object.question);
    }
    return message;
  },
};

const baseQuestionStoreServiceDeleteRequest: object = { globalId: 0 };

export const QuestionStoreServiceDeleteRequest = {
  encode(
    message: QuestionStoreServiceDeleteRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): QuestionStoreServiceDeleteRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQuestionStoreServiceDeleteRequest,
    } as QuestionStoreServiceDeleteRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QuestionStoreServiceDeleteRequest {
    const message = {
      ...baseQuestionStoreServiceDeleteRequest,
    } as QuestionStoreServiceDeleteRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    return message;
  },

  toJSON(message: QuestionStoreServiceDeleteRequest): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QuestionStoreServiceDeleteRequest>
  ): QuestionStoreServiceDeleteRequest {
    const message = {
      ...baseQuestionStoreServiceDeleteRequest,
    } as QuestionStoreServiceDeleteRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    return message;
  },
};

const baseQuestionStoreServiceDeleteResponse: object = {};

export const QuestionStoreServiceDeleteResponse = {
  encode(
    message: QuestionStoreServiceDeleteResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.question !== undefined) {
      Question.encode(message.question, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): QuestionStoreServiceDeleteResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseQuestionStoreServiceDeleteResponse,
    } as QuestionStoreServiceDeleteResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.question = Question.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): QuestionStoreServiceDeleteResponse {
    const message = {
      ...baseQuestionStoreServiceDeleteResponse,
    } as QuestionStoreServiceDeleteResponse;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromJSON(object.question);
    }
    return message;
  },

  toJSON(message: QuestionStoreServiceDeleteResponse): unknown {
    const obj: any = {};
    message.question !== undefined &&
      (obj.question = message.question
        ? Question.toJSON(message.question)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<QuestionStoreServiceDeleteResponse>
  ): QuestionStoreServiceDeleteResponse {
    const message = {
      ...baseQuestionStoreServiceDeleteResponse,
    } as QuestionStoreServiceDeleteResponse;
    if (object.question !== undefined && object.question !== null) {
      message.question = Question.fromPartial(object.question);
    }
    return message;
  },
};

export const QuestionStoreServiceService = {
  getGlobal: {
    path: "/enem.metadata.v1.QuestionStoreService/GetGlobal",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: QuestionStoreServiceGetGlobalRequest) =>
      Buffer.from(QuestionStoreServiceGetGlobalRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      QuestionStoreServiceGetGlobalRequest.decode(value),
    responseSerialize: (value: QuestionStoreServiceGetGlobalResponse) =>
      Buffer.from(QuestionStoreServiceGetGlobalResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      QuestionStoreServiceGetGlobalResponse.decode(value),
  },
  save: {
    path: "/enem.metadata.v1.QuestionStoreService/Save",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: QuestionStoreServiceSaveRequest) =>
      Buffer.from(QuestionStoreServiceSaveRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      QuestionStoreServiceSaveRequest.decode(value),
    responseSerialize: (value: QuestionStoreServiceSaveResponse) =>
      Buffer.from(QuestionStoreServiceSaveResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      QuestionStoreServiceSaveResponse.decode(value),
  },
  delete: {
    path: "/enem.metadata.v1.QuestionStoreService/Delete",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: QuestionStoreServiceDeleteRequest) =>
      Buffer.from(QuestionStoreServiceDeleteRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      QuestionStoreServiceDeleteRequest.decode(value),
    responseSerialize: (value: QuestionStoreServiceDeleteResponse) =>
      Buffer.from(QuestionStoreServiceDeleteResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      QuestionStoreServiceDeleteResponse.decode(value),
  },
} as const;

export interface QuestionStoreServiceServer
  extends UntypedServiceImplementation {
  getGlobal: handleUnaryCall<
    QuestionStoreServiceGetGlobalRequest,
    QuestionStoreServiceGetGlobalResponse
  >;
  save: handleUnaryCall<
    QuestionStoreServiceSaveRequest,
    QuestionStoreServiceSaveResponse
  >;
  delete: handleUnaryCall<
    QuestionStoreServiceDeleteRequest,
    QuestionStoreServiceDeleteResponse
  >;
}

export interface QuestionStoreServiceClient extends Client {
  getGlobal(
    request: QuestionStoreServiceGetGlobalRequest,
    callback: (
      error: ServiceError | null,
      response: QuestionStoreServiceGetGlobalResponse
    ) => void
  ): ClientUnaryCall;
  getGlobal(
    request: QuestionStoreServiceGetGlobalRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: QuestionStoreServiceGetGlobalResponse
    ) => void
  ): ClientUnaryCall;
  getGlobal(
    request: QuestionStoreServiceGetGlobalRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: QuestionStoreServiceGetGlobalResponse
    ) => void
  ): ClientUnaryCall;
  save(
    request: QuestionStoreServiceSaveRequest,
    callback: (
      error: ServiceError | null,
      response: QuestionStoreServiceSaveResponse
    ) => void
  ): ClientUnaryCall;
  save(
    request: QuestionStoreServiceSaveRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: QuestionStoreServiceSaveResponse
    ) => void
  ): ClientUnaryCall;
  save(
    request: QuestionStoreServiceSaveRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: QuestionStoreServiceSaveResponse
    ) => void
  ): ClientUnaryCall;
  delete(
    request: QuestionStoreServiceDeleteRequest,
    callback: (
      error: ServiceError | null,
      response: QuestionStoreServiceDeleteResponse
    ) => void
  ): ClientUnaryCall;
  delete(
    request: QuestionStoreServiceDeleteRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: QuestionStoreServiceDeleteResponse
    ) => void
  ): ClientUnaryCall;
  delete(
    request: QuestionStoreServiceDeleteRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: QuestionStoreServiceDeleteResponse
    ) => void
  ): ClientUnaryCall;
}

export const QuestionStoreServiceClient = makeGenericClientConstructor(
  QuestionStoreServiceService,
  "enem.metadata.v1.QuestionStoreService"
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): QuestionStoreServiceClient;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined
  | Long;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends { $case: string }
  ? { [K in keyof Omit<T, "$case">]?: DeepPartial<T[K]> } & {
      $case: T["$case"];
    }
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
