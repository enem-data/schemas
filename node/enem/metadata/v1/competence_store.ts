/* eslint-disable */
import Long from "long";
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from "@grpc/grpc-js";
import _m0 from "protobufjs/minimal";
import {
  Subject,
  Competence,
  subjectFromJSON,
  subjectToJSON,
} from "../../../enem/metadata/v1/types";

export const protobufPackage = "enem.metadata.v1";

export interface CompetenceStoreServiceGetGlobalRequest {
  globalId: number;
}

export interface CompetenceStoreServiceGetGlobalResponse {
  competence?: Competence | undefined;
}

export interface CompetenceStoreServiceGetLocalRequest {
  subject: Subject;
  localId: number;
}

export interface CompetenceStoreServiceGetLocalResponse {
  competence?: Competence | undefined;
}

export interface CompetenceStoreServiceCreateRequest {
  subject: Subject;
  description?: string | undefined;
}

export interface CompetenceStoreServiceCreateResponse {
  competence: Competence | undefined;
}

export interface CompetenceStoreServiceUpdateRequest {
  globalId: number;
  description?: string | undefined;
}

export interface CompetenceStoreServiceUpdateResponse {
  competence: Competence | undefined;
}

export interface CompetenceStoreServiceDeleteRequest {
  globalId: number;
}

export interface CompetenceStoreServiceDeleteResponse {
  competence: Competence | undefined;
}

const baseCompetenceStoreServiceGetGlobalRequest: object = { globalId: 0 };

export const CompetenceStoreServiceGetGlobalRequest = {
  encode(
    message: CompetenceStoreServiceGetGlobalRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CompetenceStoreServiceGetGlobalRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseCompetenceStoreServiceGetGlobalRequest,
    } as CompetenceStoreServiceGetGlobalRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CompetenceStoreServiceGetGlobalRequest {
    const message = {
      ...baseCompetenceStoreServiceGetGlobalRequest,
    } as CompetenceStoreServiceGetGlobalRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    return message;
  },

  toJSON(message: CompetenceStoreServiceGetGlobalRequest): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CompetenceStoreServiceGetGlobalRequest>
  ): CompetenceStoreServiceGetGlobalRequest {
    const message = {
      ...baseCompetenceStoreServiceGetGlobalRequest,
    } as CompetenceStoreServiceGetGlobalRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    return message;
  },
};

const baseCompetenceStoreServiceGetGlobalResponse: object = {};

export const CompetenceStoreServiceGetGlobalResponse = {
  encode(
    message: CompetenceStoreServiceGetGlobalResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.competence !== undefined) {
      Competence.encode(message.competence, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CompetenceStoreServiceGetGlobalResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseCompetenceStoreServiceGetGlobalResponse,
    } as CompetenceStoreServiceGetGlobalResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.competence = Competence.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CompetenceStoreServiceGetGlobalResponse {
    const message = {
      ...baseCompetenceStoreServiceGetGlobalResponse,
    } as CompetenceStoreServiceGetGlobalResponse;
    if (object.competence !== undefined && object.competence !== null) {
      message.competence = Competence.fromJSON(object.competence);
    }
    return message;
  },

  toJSON(message: CompetenceStoreServiceGetGlobalResponse): unknown {
    const obj: any = {};
    message.competence !== undefined &&
      (obj.competence = message.competence
        ? Competence.toJSON(message.competence)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CompetenceStoreServiceGetGlobalResponse>
  ): CompetenceStoreServiceGetGlobalResponse {
    const message = {
      ...baseCompetenceStoreServiceGetGlobalResponse,
    } as CompetenceStoreServiceGetGlobalResponse;
    if (object.competence !== undefined && object.competence !== null) {
      message.competence = Competence.fromPartial(object.competence);
    }
    return message;
  },
};

const baseCompetenceStoreServiceGetLocalRequest: object = {
  subject: 0,
  localId: 0,
};

export const CompetenceStoreServiceGetLocalRequest = {
  encode(
    message: CompetenceStoreServiceGetLocalRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.subject !== 0) {
      writer.uint32(8).int32(message.subject);
    }
    if (message.localId !== 0) {
      writer.uint32(16).uint32(message.localId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CompetenceStoreServiceGetLocalRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseCompetenceStoreServiceGetLocalRequest,
    } as CompetenceStoreServiceGetLocalRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.subject = reader.int32() as any;
          break;
        case 2:
          message.localId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CompetenceStoreServiceGetLocalRequest {
    const message = {
      ...baseCompetenceStoreServiceGetLocalRequest,
    } as CompetenceStoreServiceGetLocalRequest;
    if (object.subject !== undefined && object.subject !== null) {
      message.subject = subjectFromJSON(object.subject);
    }
    if (object.localId !== undefined && object.localId !== null) {
      message.localId = Number(object.localId);
    }
    return message;
  },

  toJSON(message: CompetenceStoreServiceGetLocalRequest): unknown {
    const obj: any = {};
    message.subject !== undefined &&
      (obj.subject = subjectToJSON(message.subject));
    message.localId !== undefined && (obj.localId = message.localId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CompetenceStoreServiceGetLocalRequest>
  ): CompetenceStoreServiceGetLocalRequest {
    const message = {
      ...baseCompetenceStoreServiceGetLocalRequest,
    } as CompetenceStoreServiceGetLocalRequest;
    if (object.subject !== undefined && object.subject !== null) {
      message.subject = object.subject;
    }
    if (object.localId !== undefined && object.localId !== null) {
      message.localId = object.localId;
    }
    return message;
  },
};

const baseCompetenceStoreServiceGetLocalResponse: object = {};

export const CompetenceStoreServiceGetLocalResponse = {
  encode(
    message: CompetenceStoreServiceGetLocalResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.competence !== undefined) {
      Competence.encode(message.competence, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CompetenceStoreServiceGetLocalResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseCompetenceStoreServiceGetLocalResponse,
    } as CompetenceStoreServiceGetLocalResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.competence = Competence.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CompetenceStoreServiceGetLocalResponse {
    const message = {
      ...baseCompetenceStoreServiceGetLocalResponse,
    } as CompetenceStoreServiceGetLocalResponse;
    if (object.competence !== undefined && object.competence !== null) {
      message.competence = Competence.fromJSON(object.competence);
    }
    return message;
  },

  toJSON(message: CompetenceStoreServiceGetLocalResponse): unknown {
    const obj: any = {};
    message.competence !== undefined &&
      (obj.competence = message.competence
        ? Competence.toJSON(message.competence)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CompetenceStoreServiceGetLocalResponse>
  ): CompetenceStoreServiceGetLocalResponse {
    const message = {
      ...baseCompetenceStoreServiceGetLocalResponse,
    } as CompetenceStoreServiceGetLocalResponse;
    if (object.competence !== undefined && object.competence !== null) {
      message.competence = Competence.fromPartial(object.competence);
    }
    return message;
  },
};

const baseCompetenceStoreServiceCreateRequest: object = { subject: 0 };

export const CompetenceStoreServiceCreateRequest = {
  encode(
    message: CompetenceStoreServiceCreateRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.subject !== 0) {
      writer.uint32(8).int32(message.subject);
    }
    if (message.description !== undefined) {
      writer.uint32(18).string(message.description);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CompetenceStoreServiceCreateRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseCompetenceStoreServiceCreateRequest,
    } as CompetenceStoreServiceCreateRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.subject = reader.int32() as any;
          break;
        case 2:
          message.description = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CompetenceStoreServiceCreateRequest {
    const message = {
      ...baseCompetenceStoreServiceCreateRequest,
    } as CompetenceStoreServiceCreateRequest;
    if (object.subject !== undefined && object.subject !== null) {
      message.subject = subjectFromJSON(object.subject);
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = String(object.description);
    }
    return message;
  },

  toJSON(message: CompetenceStoreServiceCreateRequest): unknown {
    const obj: any = {};
    message.subject !== undefined &&
      (obj.subject = subjectToJSON(message.subject));
    message.description !== undefined &&
      (obj.description = message.description);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CompetenceStoreServiceCreateRequest>
  ): CompetenceStoreServiceCreateRequest {
    const message = {
      ...baseCompetenceStoreServiceCreateRequest,
    } as CompetenceStoreServiceCreateRequest;
    if (object.subject !== undefined && object.subject !== null) {
      message.subject = object.subject;
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = object.description;
    }
    return message;
  },
};

const baseCompetenceStoreServiceCreateResponse: object = {};

export const CompetenceStoreServiceCreateResponse = {
  encode(
    message: CompetenceStoreServiceCreateResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.competence !== undefined) {
      Competence.encode(message.competence, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CompetenceStoreServiceCreateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseCompetenceStoreServiceCreateResponse,
    } as CompetenceStoreServiceCreateResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.competence = Competence.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CompetenceStoreServiceCreateResponse {
    const message = {
      ...baseCompetenceStoreServiceCreateResponse,
    } as CompetenceStoreServiceCreateResponse;
    if (object.competence !== undefined && object.competence !== null) {
      message.competence = Competence.fromJSON(object.competence);
    }
    return message;
  },

  toJSON(message: CompetenceStoreServiceCreateResponse): unknown {
    const obj: any = {};
    message.competence !== undefined &&
      (obj.competence = message.competence
        ? Competence.toJSON(message.competence)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CompetenceStoreServiceCreateResponse>
  ): CompetenceStoreServiceCreateResponse {
    const message = {
      ...baseCompetenceStoreServiceCreateResponse,
    } as CompetenceStoreServiceCreateResponse;
    if (object.competence !== undefined && object.competence !== null) {
      message.competence = Competence.fromPartial(object.competence);
    }
    return message;
  },
};

const baseCompetenceStoreServiceUpdateRequest: object = { globalId: 0 };

export const CompetenceStoreServiceUpdateRequest = {
  encode(
    message: CompetenceStoreServiceUpdateRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    if (message.description !== undefined) {
      writer.uint32(18).string(message.description);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CompetenceStoreServiceUpdateRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseCompetenceStoreServiceUpdateRequest,
    } as CompetenceStoreServiceUpdateRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        case 2:
          message.description = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CompetenceStoreServiceUpdateRequest {
    const message = {
      ...baseCompetenceStoreServiceUpdateRequest,
    } as CompetenceStoreServiceUpdateRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = String(object.description);
    }
    return message;
  },

  toJSON(message: CompetenceStoreServiceUpdateRequest): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    message.description !== undefined &&
      (obj.description = message.description);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CompetenceStoreServiceUpdateRequest>
  ): CompetenceStoreServiceUpdateRequest {
    const message = {
      ...baseCompetenceStoreServiceUpdateRequest,
    } as CompetenceStoreServiceUpdateRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    if (object.description !== undefined && object.description !== null) {
      message.description = object.description;
    }
    return message;
  },
};

const baseCompetenceStoreServiceUpdateResponse: object = {};

export const CompetenceStoreServiceUpdateResponse = {
  encode(
    message: CompetenceStoreServiceUpdateResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.competence !== undefined) {
      Competence.encode(message.competence, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CompetenceStoreServiceUpdateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseCompetenceStoreServiceUpdateResponse,
    } as CompetenceStoreServiceUpdateResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.competence = Competence.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CompetenceStoreServiceUpdateResponse {
    const message = {
      ...baseCompetenceStoreServiceUpdateResponse,
    } as CompetenceStoreServiceUpdateResponse;
    if (object.competence !== undefined && object.competence !== null) {
      message.competence = Competence.fromJSON(object.competence);
    }
    return message;
  },

  toJSON(message: CompetenceStoreServiceUpdateResponse): unknown {
    const obj: any = {};
    message.competence !== undefined &&
      (obj.competence = message.competence
        ? Competence.toJSON(message.competence)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CompetenceStoreServiceUpdateResponse>
  ): CompetenceStoreServiceUpdateResponse {
    const message = {
      ...baseCompetenceStoreServiceUpdateResponse,
    } as CompetenceStoreServiceUpdateResponse;
    if (object.competence !== undefined && object.competence !== null) {
      message.competence = Competence.fromPartial(object.competence);
    }
    return message;
  },
};

const baseCompetenceStoreServiceDeleteRequest: object = { globalId: 0 };

export const CompetenceStoreServiceDeleteRequest = {
  encode(
    message: CompetenceStoreServiceDeleteRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CompetenceStoreServiceDeleteRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseCompetenceStoreServiceDeleteRequest,
    } as CompetenceStoreServiceDeleteRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CompetenceStoreServiceDeleteRequest {
    const message = {
      ...baseCompetenceStoreServiceDeleteRequest,
    } as CompetenceStoreServiceDeleteRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    return message;
  },

  toJSON(message: CompetenceStoreServiceDeleteRequest): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CompetenceStoreServiceDeleteRequest>
  ): CompetenceStoreServiceDeleteRequest {
    const message = {
      ...baseCompetenceStoreServiceDeleteRequest,
    } as CompetenceStoreServiceDeleteRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    return message;
  },
};

const baseCompetenceStoreServiceDeleteResponse: object = {};

export const CompetenceStoreServiceDeleteResponse = {
  encode(
    message: CompetenceStoreServiceDeleteResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.competence !== undefined) {
      Competence.encode(message.competence, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CompetenceStoreServiceDeleteResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseCompetenceStoreServiceDeleteResponse,
    } as CompetenceStoreServiceDeleteResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.competence = Competence.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CompetenceStoreServiceDeleteResponse {
    const message = {
      ...baseCompetenceStoreServiceDeleteResponse,
    } as CompetenceStoreServiceDeleteResponse;
    if (object.competence !== undefined && object.competence !== null) {
      message.competence = Competence.fromJSON(object.competence);
    }
    return message;
  },

  toJSON(message: CompetenceStoreServiceDeleteResponse): unknown {
    const obj: any = {};
    message.competence !== undefined &&
      (obj.competence = message.competence
        ? Competence.toJSON(message.competence)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CompetenceStoreServiceDeleteResponse>
  ): CompetenceStoreServiceDeleteResponse {
    const message = {
      ...baseCompetenceStoreServiceDeleteResponse,
    } as CompetenceStoreServiceDeleteResponse;
    if (object.competence !== undefined && object.competence !== null) {
      message.competence = Competence.fromPartial(object.competence);
    }
    return message;
  },
};

export const CompetenceStoreServiceService = {
  getGlobal: {
    path: "/enem.metadata.v1.CompetenceStoreService/GetGlobal",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CompetenceStoreServiceGetGlobalRequest) =>
      Buffer.from(
        CompetenceStoreServiceGetGlobalRequest.encode(value).finish()
      ),
    requestDeserialize: (value: Buffer) =>
      CompetenceStoreServiceGetGlobalRequest.decode(value),
    responseSerialize: (value: CompetenceStoreServiceGetGlobalResponse) =>
      Buffer.from(
        CompetenceStoreServiceGetGlobalResponse.encode(value).finish()
      ),
    responseDeserialize: (value: Buffer) =>
      CompetenceStoreServiceGetGlobalResponse.decode(value),
  },
  getLocal: {
    path: "/enem.metadata.v1.CompetenceStoreService/GetLocal",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CompetenceStoreServiceGetLocalRequest) =>
      Buffer.from(CompetenceStoreServiceGetLocalRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      CompetenceStoreServiceGetLocalRequest.decode(value),
    responseSerialize: (value: CompetenceStoreServiceGetLocalResponse) =>
      Buffer.from(
        CompetenceStoreServiceGetLocalResponse.encode(value).finish()
      ),
    responseDeserialize: (value: Buffer) =>
      CompetenceStoreServiceGetLocalResponse.decode(value),
  },
  create: {
    path: "/enem.metadata.v1.CompetenceStoreService/Create",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CompetenceStoreServiceCreateRequest) =>
      Buffer.from(CompetenceStoreServiceCreateRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      CompetenceStoreServiceCreateRequest.decode(value),
    responseSerialize: (value: CompetenceStoreServiceCreateResponse) =>
      Buffer.from(CompetenceStoreServiceCreateResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      CompetenceStoreServiceCreateResponse.decode(value),
  },
  update: {
    path: "/enem.metadata.v1.CompetenceStoreService/Update",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CompetenceStoreServiceUpdateRequest) =>
      Buffer.from(CompetenceStoreServiceUpdateRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      CompetenceStoreServiceUpdateRequest.decode(value),
    responseSerialize: (value: CompetenceStoreServiceUpdateResponse) =>
      Buffer.from(CompetenceStoreServiceUpdateResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      CompetenceStoreServiceUpdateResponse.decode(value),
  },
  delete: {
    path: "/enem.metadata.v1.CompetenceStoreService/Delete",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CompetenceStoreServiceDeleteRequest) =>
      Buffer.from(CompetenceStoreServiceDeleteRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      CompetenceStoreServiceDeleteRequest.decode(value),
    responseSerialize: (value: CompetenceStoreServiceDeleteResponse) =>
      Buffer.from(CompetenceStoreServiceDeleteResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      CompetenceStoreServiceDeleteResponse.decode(value),
  },
} as const;

export interface CompetenceStoreServiceServer
  extends UntypedServiceImplementation {
  getGlobal: handleUnaryCall<
    CompetenceStoreServiceGetGlobalRequest,
    CompetenceStoreServiceGetGlobalResponse
  >;
  getLocal: handleUnaryCall<
    CompetenceStoreServiceGetLocalRequest,
    CompetenceStoreServiceGetLocalResponse
  >;
  create: handleUnaryCall<
    CompetenceStoreServiceCreateRequest,
    CompetenceStoreServiceCreateResponse
  >;
  update: handleUnaryCall<
    CompetenceStoreServiceUpdateRequest,
    CompetenceStoreServiceUpdateResponse
  >;
  delete: handleUnaryCall<
    CompetenceStoreServiceDeleteRequest,
    CompetenceStoreServiceDeleteResponse
  >;
}

export interface CompetenceStoreServiceClient extends Client {
  getGlobal(
    request: CompetenceStoreServiceGetGlobalRequest,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceGetGlobalResponse
    ) => void
  ): ClientUnaryCall;
  getGlobal(
    request: CompetenceStoreServiceGetGlobalRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceGetGlobalResponse
    ) => void
  ): ClientUnaryCall;
  getGlobal(
    request: CompetenceStoreServiceGetGlobalRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceGetGlobalResponse
    ) => void
  ): ClientUnaryCall;
  getLocal(
    request: CompetenceStoreServiceGetLocalRequest,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceGetLocalResponse
    ) => void
  ): ClientUnaryCall;
  getLocal(
    request: CompetenceStoreServiceGetLocalRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceGetLocalResponse
    ) => void
  ): ClientUnaryCall;
  getLocal(
    request: CompetenceStoreServiceGetLocalRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceGetLocalResponse
    ) => void
  ): ClientUnaryCall;
  create(
    request: CompetenceStoreServiceCreateRequest,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceCreateResponse
    ) => void
  ): ClientUnaryCall;
  create(
    request: CompetenceStoreServiceCreateRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceCreateResponse
    ) => void
  ): ClientUnaryCall;
  create(
    request: CompetenceStoreServiceCreateRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceCreateResponse
    ) => void
  ): ClientUnaryCall;
  update(
    request: CompetenceStoreServiceUpdateRequest,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceUpdateResponse
    ) => void
  ): ClientUnaryCall;
  update(
    request: CompetenceStoreServiceUpdateRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceUpdateResponse
    ) => void
  ): ClientUnaryCall;
  update(
    request: CompetenceStoreServiceUpdateRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceUpdateResponse
    ) => void
  ): ClientUnaryCall;
  delete(
    request: CompetenceStoreServiceDeleteRequest,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceDeleteResponse
    ) => void
  ): ClientUnaryCall;
  delete(
    request: CompetenceStoreServiceDeleteRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceDeleteResponse
    ) => void
  ): ClientUnaryCall;
  delete(
    request: CompetenceStoreServiceDeleteRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CompetenceStoreServiceDeleteResponse
    ) => void
  ): ClientUnaryCall;
}

export const CompetenceStoreServiceClient = makeGenericClientConstructor(
  CompetenceStoreServiceService,
  "enem.metadata.v1.CompetenceStoreService"
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): CompetenceStoreServiceClient;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined
  | Long;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends { $case: string }
  ? { [K in keyof Omit<T, "$case">]?: DeepPartial<T[K]> } & {
      $case: T["$case"];
    }
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
