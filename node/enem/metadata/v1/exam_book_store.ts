/* eslint-disable */
import Long from "long";
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from "@grpc/grpc-js";
import _m0 from "protobufjs/minimal";
import {
  Subject,
  ExamBook,
  Question,
  subjectFromJSON,
  subjectToJSON,
} from "../../../enem/metadata/v1/types";

export const protobufPackage = "enem.metadata.v1";

export interface ExamBookStoreServiceGetGlobalRequest {
  globalId: number;
}

export interface ExamBookStoreServiceGetGlobalResponse {
  examBook?: ExamBook | undefined;
}

export interface ExamBookStoreServiceGetLocalRequest {
  year: number;
  localId: number;
}

export interface ExamBookStoreServiceGetLocalResponse {
  examBook?: ExamBook | undefined;
}

export interface ExamBookStoreServiceCreateRequest {
  year: number;
  /** The INEP ID for this particular ExamBook. These are unique within the same year. */
  localId: number;
  /**
   * The color of the ExamBook, used to identify the ordering of the questions by the participants.
   * Here the value is normalized to full lower case.
   */
  color: string;
  subject: Subject;
  /**
   * An ExamBook of SUBJECT_LC must have 50 questions in this order:
   *  - English specific questions, numbered 1 - 5
   *  - Spanish specific questions, numbered 1 - 5
   *  - Native language questions, numbered 6 - 45
   *
   * ExamBooks of any other subject must have exactly 45 questions.
   */
  questions: Question[];
}

export interface ExamBookStoreServiceCreateResponse {
  examBook: ExamBook | undefined;
}

export interface ExamBookStoreServiceUpdateRequest {
  globalId: number;
  subject?: Subject | undefined;
  /**
   * An ExamBook of SUBJECT_LC must have 50 questions in this order:
   *  - English specific questions, numbered 1 - 5
   *  - Spanish specific questions, numbered 1 - 5
   *  - Native language questions, numbered 6 - 45
   *
   * ExamBooks of any other subject must have exactly 45 questions.
   *
   * Passing an empty list will keep the current questions.
   */
  questions: Question[];
}

export interface ExamBookStoreServiceUpdateResponse {
  examBook: ExamBook | undefined;
}

export interface ExamBookStoreServiceDeleteRequest {
  globalId: number;
}

export interface ExamBookStoreServiceDeleteResponse {
  examBook: ExamBook | undefined;
}

const baseExamBookStoreServiceGetGlobalRequest: object = { globalId: 0 };

export const ExamBookStoreServiceGetGlobalRequest = {
  encode(
    message: ExamBookStoreServiceGetGlobalRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ExamBookStoreServiceGetGlobalRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseExamBookStoreServiceGetGlobalRequest,
    } as ExamBookStoreServiceGetGlobalRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExamBookStoreServiceGetGlobalRequest {
    const message = {
      ...baseExamBookStoreServiceGetGlobalRequest,
    } as ExamBookStoreServiceGetGlobalRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    return message;
  },

  toJSON(message: ExamBookStoreServiceGetGlobalRequest): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ExamBookStoreServiceGetGlobalRequest>
  ): ExamBookStoreServiceGetGlobalRequest {
    const message = {
      ...baseExamBookStoreServiceGetGlobalRequest,
    } as ExamBookStoreServiceGetGlobalRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    return message;
  },
};

const baseExamBookStoreServiceGetGlobalResponse: object = {};

export const ExamBookStoreServiceGetGlobalResponse = {
  encode(
    message: ExamBookStoreServiceGetGlobalResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examBook !== undefined) {
      ExamBook.encode(message.examBook, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ExamBookStoreServiceGetGlobalResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseExamBookStoreServiceGetGlobalResponse,
    } as ExamBookStoreServiceGetGlobalResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.examBook = ExamBook.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExamBookStoreServiceGetGlobalResponse {
    const message = {
      ...baseExamBookStoreServiceGetGlobalResponse,
    } as ExamBookStoreServiceGetGlobalResponse;
    if (object.examBook !== undefined && object.examBook !== null) {
      message.examBook = ExamBook.fromJSON(object.examBook);
    }
    return message;
  },

  toJSON(message: ExamBookStoreServiceGetGlobalResponse): unknown {
    const obj: any = {};
    message.examBook !== undefined &&
      (obj.examBook = message.examBook
        ? ExamBook.toJSON(message.examBook)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ExamBookStoreServiceGetGlobalResponse>
  ): ExamBookStoreServiceGetGlobalResponse {
    const message = {
      ...baseExamBookStoreServiceGetGlobalResponse,
    } as ExamBookStoreServiceGetGlobalResponse;
    if (object.examBook !== undefined && object.examBook !== null) {
      message.examBook = ExamBook.fromPartial(object.examBook);
    }
    return message;
  },
};

const baseExamBookStoreServiceGetLocalRequest: object = { year: 0, localId: 0 };

export const ExamBookStoreServiceGetLocalRequest = {
  encode(
    message: ExamBookStoreServiceGetLocalRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.year !== 0) {
      writer.uint32(8).uint32(message.year);
    }
    if (message.localId !== 0) {
      writer.uint32(16).uint32(message.localId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ExamBookStoreServiceGetLocalRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseExamBookStoreServiceGetLocalRequest,
    } as ExamBookStoreServiceGetLocalRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.year = reader.uint32();
          break;
        case 2:
          message.localId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExamBookStoreServiceGetLocalRequest {
    const message = {
      ...baseExamBookStoreServiceGetLocalRequest,
    } as ExamBookStoreServiceGetLocalRequest;
    if (object.year !== undefined && object.year !== null) {
      message.year = Number(object.year);
    }
    if (object.localId !== undefined && object.localId !== null) {
      message.localId = Number(object.localId);
    }
    return message;
  },

  toJSON(message: ExamBookStoreServiceGetLocalRequest): unknown {
    const obj: any = {};
    message.year !== undefined && (obj.year = message.year);
    message.localId !== undefined && (obj.localId = message.localId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ExamBookStoreServiceGetLocalRequest>
  ): ExamBookStoreServiceGetLocalRequest {
    const message = {
      ...baseExamBookStoreServiceGetLocalRequest,
    } as ExamBookStoreServiceGetLocalRequest;
    if (object.year !== undefined && object.year !== null) {
      message.year = object.year;
    }
    if (object.localId !== undefined && object.localId !== null) {
      message.localId = object.localId;
    }
    return message;
  },
};

const baseExamBookStoreServiceGetLocalResponse: object = {};

export const ExamBookStoreServiceGetLocalResponse = {
  encode(
    message: ExamBookStoreServiceGetLocalResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examBook !== undefined) {
      ExamBook.encode(message.examBook, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ExamBookStoreServiceGetLocalResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseExamBookStoreServiceGetLocalResponse,
    } as ExamBookStoreServiceGetLocalResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.examBook = ExamBook.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExamBookStoreServiceGetLocalResponse {
    const message = {
      ...baseExamBookStoreServiceGetLocalResponse,
    } as ExamBookStoreServiceGetLocalResponse;
    if (object.examBook !== undefined && object.examBook !== null) {
      message.examBook = ExamBook.fromJSON(object.examBook);
    }
    return message;
  },

  toJSON(message: ExamBookStoreServiceGetLocalResponse): unknown {
    const obj: any = {};
    message.examBook !== undefined &&
      (obj.examBook = message.examBook
        ? ExamBook.toJSON(message.examBook)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ExamBookStoreServiceGetLocalResponse>
  ): ExamBookStoreServiceGetLocalResponse {
    const message = {
      ...baseExamBookStoreServiceGetLocalResponse,
    } as ExamBookStoreServiceGetLocalResponse;
    if (object.examBook !== undefined && object.examBook !== null) {
      message.examBook = ExamBook.fromPartial(object.examBook);
    }
    return message;
  },
};

const baseExamBookStoreServiceCreateRequest: object = {
  year: 0,
  localId: 0,
  color: "",
  subject: 0,
};

export const ExamBookStoreServiceCreateRequest = {
  encode(
    message: ExamBookStoreServiceCreateRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.year !== 0) {
      writer.uint32(16).uint32(message.year);
    }
    if (message.localId !== 0) {
      writer.uint32(24).uint32(message.localId);
    }
    if (message.color !== "") {
      writer.uint32(34).string(message.color);
    }
    if (message.subject !== 0) {
      writer.uint32(40).int32(message.subject);
    }
    for (const v of message.questions) {
      Question.encode(v!, writer.uint32(50).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ExamBookStoreServiceCreateRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseExamBookStoreServiceCreateRequest,
    } as ExamBookStoreServiceCreateRequest;
    message.questions = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 2:
          message.year = reader.uint32();
          break;
        case 3:
          message.localId = reader.uint32();
          break;
        case 4:
          message.color = reader.string();
          break;
        case 5:
          message.subject = reader.int32() as any;
          break;
        case 6:
          message.questions.push(Question.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExamBookStoreServiceCreateRequest {
    const message = {
      ...baseExamBookStoreServiceCreateRequest,
    } as ExamBookStoreServiceCreateRequest;
    message.questions = [];
    if (object.year !== undefined && object.year !== null) {
      message.year = Number(object.year);
    }
    if (object.localId !== undefined && object.localId !== null) {
      message.localId = Number(object.localId);
    }
    if (object.color !== undefined && object.color !== null) {
      message.color = String(object.color);
    }
    if (object.subject !== undefined && object.subject !== null) {
      message.subject = subjectFromJSON(object.subject);
    }
    if (object.questions !== undefined && object.questions !== null) {
      for (const e of object.questions) {
        message.questions.push(Question.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: ExamBookStoreServiceCreateRequest): unknown {
    const obj: any = {};
    message.year !== undefined && (obj.year = message.year);
    message.localId !== undefined && (obj.localId = message.localId);
    message.color !== undefined && (obj.color = message.color);
    message.subject !== undefined &&
      (obj.subject = subjectToJSON(message.subject));
    if (message.questions) {
      obj.questions = message.questions.map((e) =>
        e ? Question.toJSON(e) : undefined
      );
    } else {
      obj.questions = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<ExamBookStoreServiceCreateRequest>
  ): ExamBookStoreServiceCreateRequest {
    const message = {
      ...baseExamBookStoreServiceCreateRequest,
    } as ExamBookStoreServiceCreateRequest;
    message.questions = [];
    if (object.year !== undefined && object.year !== null) {
      message.year = object.year;
    }
    if (object.localId !== undefined && object.localId !== null) {
      message.localId = object.localId;
    }
    if (object.color !== undefined && object.color !== null) {
      message.color = object.color;
    }
    if (object.subject !== undefined && object.subject !== null) {
      message.subject = object.subject;
    }
    if (object.questions !== undefined && object.questions !== null) {
      for (const e of object.questions) {
        message.questions.push(Question.fromPartial(e));
      }
    }
    return message;
  },
};

const baseExamBookStoreServiceCreateResponse: object = {};

export const ExamBookStoreServiceCreateResponse = {
  encode(
    message: ExamBookStoreServiceCreateResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examBook !== undefined) {
      ExamBook.encode(message.examBook, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ExamBookStoreServiceCreateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseExamBookStoreServiceCreateResponse,
    } as ExamBookStoreServiceCreateResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.examBook = ExamBook.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExamBookStoreServiceCreateResponse {
    const message = {
      ...baseExamBookStoreServiceCreateResponse,
    } as ExamBookStoreServiceCreateResponse;
    if (object.examBook !== undefined && object.examBook !== null) {
      message.examBook = ExamBook.fromJSON(object.examBook);
    }
    return message;
  },

  toJSON(message: ExamBookStoreServiceCreateResponse): unknown {
    const obj: any = {};
    message.examBook !== undefined &&
      (obj.examBook = message.examBook
        ? ExamBook.toJSON(message.examBook)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ExamBookStoreServiceCreateResponse>
  ): ExamBookStoreServiceCreateResponse {
    const message = {
      ...baseExamBookStoreServiceCreateResponse,
    } as ExamBookStoreServiceCreateResponse;
    if (object.examBook !== undefined && object.examBook !== null) {
      message.examBook = ExamBook.fromPartial(object.examBook);
    }
    return message;
  },
};

const baseExamBookStoreServiceUpdateRequest: object = { globalId: 0 };

export const ExamBookStoreServiceUpdateRequest = {
  encode(
    message: ExamBookStoreServiceUpdateRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    if (message.subject !== undefined) {
      writer.uint32(40).int32(message.subject);
    }
    for (const v of message.questions) {
      Question.encode(v!, writer.uint32(50).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ExamBookStoreServiceUpdateRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseExamBookStoreServiceUpdateRequest,
    } as ExamBookStoreServiceUpdateRequest;
    message.questions = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        case 5:
          message.subject = reader.int32() as any;
          break;
        case 6:
          message.questions.push(Question.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExamBookStoreServiceUpdateRequest {
    const message = {
      ...baseExamBookStoreServiceUpdateRequest,
    } as ExamBookStoreServiceUpdateRequest;
    message.questions = [];
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    if (object.subject !== undefined && object.subject !== null) {
      message.subject = subjectFromJSON(object.subject);
    }
    if (object.questions !== undefined && object.questions !== null) {
      for (const e of object.questions) {
        message.questions.push(Question.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: ExamBookStoreServiceUpdateRequest): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    message.subject !== undefined &&
      (obj.subject =
        message.subject !== undefined
          ? subjectToJSON(message.subject)
          : undefined);
    if (message.questions) {
      obj.questions = message.questions.map((e) =>
        e ? Question.toJSON(e) : undefined
      );
    } else {
      obj.questions = [];
    }
    return obj;
  },

  fromPartial(
    object: DeepPartial<ExamBookStoreServiceUpdateRequest>
  ): ExamBookStoreServiceUpdateRequest {
    const message = {
      ...baseExamBookStoreServiceUpdateRequest,
    } as ExamBookStoreServiceUpdateRequest;
    message.questions = [];
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    if (object.subject !== undefined && object.subject !== null) {
      message.subject = object.subject;
    }
    if (object.questions !== undefined && object.questions !== null) {
      for (const e of object.questions) {
        message.questions.push(Question.fromPartial(e));
      }
    }
    return message;
  },
};

const baseExamBookStoreServiceUpdateResponse: object = {};

export const ExamBookStoreServiceUpdateResponse = {
  encode(
    message: ExamBookStoreServiceUpdateResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examBook !== undefined) {
      ExamBook.encode(message.examBook, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ExamBookStoreServiceUpdateResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseExamBookStoreServiceUpdateResponse,
    } as ExamBookStoreServiceUpdateResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.examBook = ExamBook.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExamBookStoreServiceUpdateResponse {
    const message = {
      ...baseExamBookStoreServiceUpdateResponse,
    } as ExamBookStoreServiceUpdateResponse;
    if (object.examBook !== undefined && object.examBook !== null) {
      message.examBook = ExamBook.fromJSON(object.examBook);
    }
    return message;
  },

  toJSON(message: ExamBookStoreServiceUpdateResponse): unknown {
    const obj: any = {};
    message.examBook !== undefined &&
      (obj.examBook = message.examBook
        ? ExamBook.toJSON(message.examBook)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ExamBookStoreServiceUpdateResponse>
  ): ExamBookStoreServiceUpdateResponse {
    const message = {
      ...baseExamBookStoreServiceUpdateResponse,
    } as ExamBookStoreServiceUpdateResponse;
    if (object.examBook !== undefined && object.examBook !== null) {
      message.examBook = ExamBook.fromPartial(object.examBook);
    }
    return message;
  },
};

const baseExamBookStoreServiceDeleteRequest: object = { globalId: 0 };

export const ExamBookStoreServiceDeleteRequest = {
  encode(
    message: ExamBookStoreServiceDeleteRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.globalId !== 0) {
      writer.uint32(8).uint32(message.globalId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ExamBookStoreServiceDeleteRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseExamBookStoreServiceDeleteRequest,
    } as ExamBookStoreServiceDeleteRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.globalId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExamBookStoreServiceDeleteRequest {
    const message = {
      ...baseExamBookStoreServiceDeleteRequest,
    } as ExamBookStoreServiceDeleteRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = Number(object.globalId);
    }
    return message;
  },

  toJSON(message: ExamBookStoreServiceDeleteRequest): unknown {
    const obj: any = {};
    message.globalId !== undefined && (obj.globalId = message.globalId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ExamBookStoreServiceDeleteRequest>
  ): ExamBookStoreServiceDeleteRequest {
    const message = {
      ...baseExamBookStoreServiceDeleteRequest,
    } as ExamBookStoreServiceDeleteRequest;
    if (object.globalId !== undefined && object.globalId !== null) {
      message.globalId = object.globalId;
    }
    return message;
  },
};

const baseExamBookStoreServiceDeleteResponse: object = {};

export const ExamBookStoreServiceDeleteResponse = {
  encode(
    message: ExamBookStoreServiceDeleteResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examBook !== undefined) {
      ExamBook.encode(message.examBook, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ExamBookStoreServiceDeleteResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseExamBookStoreServiceDeleteResponse,
    } as ExamBookStoreServiceDeleteResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.examBook = ExamBook.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExamBookStoreServiceDeleteResponse {
    const message = {
      ...baseExamBookStoreServiceDeleteResponse,
    } as ExamBookStoreServiceDeleteResponse;
    if (object.examBook !== undefined && object.examBook !== null) {
      message.examBook = ExamBook.fromJSON(object.examBook);
    }
    return message;
  },

  toJSON(message: ExamBookStoreServiceDeleteResponse): unknown {
    const obj: any = {};
    message.examBook !== undefined &&
      (obj.examBook = message.examBook
        ? ExamBook.toJSON(message.examBook)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ExamBookStoreServiceDeleteResponse>
  ): ExamBookStoreServiceDeleteResponse {
    const message = {
      ...baseExamBookStoreServiceDeleteResponse,
    } as ExamBookStoreServiceDeleteResponse;
    if (object.examBook !== undefined && object.examBook !== null) {
      message.examBook = ExamBook.fromPartial(object.examBook);
    }
    return message;
  },
};

export const ExamBookStoreServiceService = {
  getGlobal: {
    path: "/enem.metadata.v1.ExamBookStoreService/GetGlobal",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ExamBookStoreServiceGetGlobalRequest) =>
      Buffer.from(ExamBookStoreServiceGetGlobalRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      ExamBookStoreServiceGetGlobalRequest.decode(value),
    responseSerialize: (value: ExamBookStoreServiceGetGlobalResponse) =>
      Buffer.from(ExamBookStoreServiceGetGlobalResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      ExamBookStoreServiceGetGlobalResponse.decode(value),
  },
  getLocal: {
    path: "/enem.metadata.v1.ExamBookStoreService/GetLocal",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ExamBookStoreServiceGetLocalRequest) =>
      Buffer.from(ExamBookStoreServiceGetLocalRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      ExamBookStoreServiceGetLocalRequest.decode(value),
    responseSerialize: (value: ExamBookStoreServiceGetLocalResponse) =>
      Buffer.from(ExamBookStoreServiceGetLocalResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      ExamBookStoreServiceGetLocalResponse.decode(value),
  },
  create: {
    path: "/enem.metadata.v1.ExamBookStoreService/Create",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ExamBookStoreServiceCreateRequest) =>
      Buffer.from(ExamBookStoreServiceCreateRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      ExamBookStoreServiceCreateRequest.decode(value),
    responseSerialize: (value: ExamBookStoreServiceCreateResponse) =>
      Buffer.from(ExamBookStoreServiceCreateResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      ExamBookStoreServiceCreateResponse.decode(value),
  },
  update: {
    path: "/enem.metadata.v1.ExamBookStoreService/Update",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ExamBookStoreServiceUpdateRequest) =>
      Buffer.from(ExamBookStoreServiceUpdateRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      ExamBookStoreServiceUpdateRequest.decode(value),
    responseSerialize: (value: ExamBookStoreServiceUpdateResponse) =>
      Buffer.from(ExamBookStoreServiceUpdateResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      ExamBookStoreServiceUpdateResponse.decode(value),
  },
  delete: {
    path: "/enem.metadata.v1.ExamBookStoreService/Delete",
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ExamBookStoreServiceDeleteRequest) =>
      Buffer.from(ExamBookStoreServiceDeleteRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) =>
      ExamBookStoreServiceDeleteRequest.decode(value),
    responseSerialize: (value: ExamBookStoreServiceDeleteResponse) =>
      Buffer.from(ExamBookStoreServiceDeleteResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) =>
      ExamBookStoreServiceDeleteResponse.decode(value),
  },
} as const;

export interface ExamBookStoreServiceServer
  extends UntypedServiceImplementation {
  getGlobal: handleUnaryCall<
    ExamBookStoreServiceGetGlobalRequest,
    ExamBookStoreServiceGetGlobalResponse
  >;
  getLocal: handleUnaryCall<
    ExamBookStoreServiceGetLocalRequest,
    ExamBookStoreServiceGetLocalResponse
  >;
  create: handleUnaryCall<
    ExamBookStoreServiceCreateRequest,
    ExamBookStoreServiceCreateResponse
  >;
  update: handleUnaryCall<
    ExamBookStoreServiceUpdateRequest,
    ExamBookStoreServiceUpdateResponse
  >;
  delete: handleUnaryCall<
    ExamBookStoreServiceDeleteRequest,
    ExamBookStoreServiceDeleteResponse
  >;
}

export interface ExamBookStoreServiceClient extends Client {
  getGlobal(
    request: ExamBookStoreServiceGetGlobalRequest,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceGetGlobalResponse
    ) => void
  ): ClientUnaryCall;
  getGlobal(
    request: ExamBookStoreServiceGetGlobalRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceGetGlobalResponse
    ) => void
  ): ClientUnaryCall;
  getGlobal(
    request: ExamBookStoreServiceGetGlobalRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceGetGlobalResponse
    ) => void
  ): ClientUnaryCall;
  getLocal(
    request: ExamBookStoreServiceGetLocalRequest,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceGetLocalResponse
    ) => void
  ): ClientUnaryCall;
  getLocal(
    request: ExamBookStoreServiceGetLocalRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceGetLocalResponse
    ) => void
  ): ClientUnaryCall;
  getLocal(
    request: ExamBookStoreServiceGetLocalRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceGetLocalResponse
    ) => void
  ): ClientUnaryCall;
  create(
    request: ExamBookStoreServiceCreateRequest,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceCreateResponse
    ) => void
  ): ClientUnaryCall;
  create(
    request: ExamBookStoreServiceCreateRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceCreateResponse
    ) => void
  ): ClientUnaryCall;
  create(
    request: ExamBookStoreServiceCreateRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceCreateResponse
    ) => void
  ): ClientUnaryCall;
  update(
    request: ExamBookStoreServiceUpdateRequest,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceUpdateResponse
    ) => void
  ): ClientUnaryCall;
  update(
    request: ExamBookStoreServiceUpdateRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceUpdateResponse
    ) => void
  ): ClientUnaryCall;
  update(
    request: ExamBookStoreServiceUpdateRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceUpdateResponse
    ) => void
  ): ClientUnaryCall;
  delete(
    request: ExamBookStoreServiceDeleteRequest,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceDeleteResponse
    ) => void
  ): ClientUnaryCall;
  delete(
    request: ExamBookStoreServiceDeleteRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceDeleteResponse
    ) => void
  ): ClientUnaryCall;
  delete(
    request: ExamBookStoreServiceDeleteRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ExamBookStoreServiceDeleteResponse
    ) => void
  ): ClientUnaryCall;
}

export const ExamBookStoreServiceClient = makeGenericClientConstructor(
  ExamBookStoreServiceService,
  "enem.metadata.v1.ExamBookStoreService"
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): ExamBookStoreServiceClient;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined
  | Long;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends { $case: string }
  ? { [K in keyof Omit<T, "$case">]?: DeepPartial<T[K]> } & {
      $case: T["$case"];
    }
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
