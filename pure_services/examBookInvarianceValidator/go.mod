module examBookInvarianceValidator

go 1.16

require (
	github.com/golang/mock v1.6.0
	github.com/mitchellh/mapstructure v1.4.1
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.0
	gitlab.com/enem-data/schemas/go v0.0.0
	google.golang.org/grpc v1.40.0
)

replace gitlab.com/enem-data/schemas/go v0.0.0 => ../../go
