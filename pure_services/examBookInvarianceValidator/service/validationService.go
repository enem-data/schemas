package service

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/enem-data/schemas/go/metadata_v1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var _ metadata_v1.ExamBookStoreServiceServer = new(ValidationService)

type ValidationService struct {
	upstreamClient metadata_v1.ExamBookStoreServiceClient
	metadata_v1.UnsafeExamBookStoreServiceServer
}

func NewValidationService(upstreamClient metadata_v1.ExamBookStoreServiceClient) *ValidationService {
	return &ValidationService{
		upstreamClient: upstreamClient,
	}
}

func (s *ValidationService) GetGlobal(ctx context.Context, request *metadata_v1.ExamBookStoreServiceGetGlobalRequest) (*metadata_v1.ExamBookStoreServiceGetGlobalResponse, error) {
	meta, _ := metadata.FromIncomingContext(ctx)
	ctx = metadata.NewOutgoingContext(ctx, meta)

	res, err := s.upstreamClient.GetGlobal(ctx, request)
	if err != nil {
		return nil, err
	}

	err = validateMessageWithExamBook(res)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Broken invariant from underlying system: %+v", err)
	}

	return res, nil
}

func (s *ValidationService) GetLocal(ctx context.Context, request *metadata_v1.ExamBookStoreServiceGetLocalRequest) (*metadata_v1.ExamBookStoreServiceGetLocalResponse, error) {
	meta, _ := metadata.FromIncomingContext(ctx)
	ctx = metadata.NewOutgoingContext(ctx, meta)

	res, err := s.upstreamClient.GetLocal(ctx, request)
	if err != nil {
		return nil, err
	}

	err = validateMessageWithExamBook(res)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Broken invariant from underlying system: %+v", err)
	}

	return res, nil
}

func (s *ValidationService) Create(ctx context.Context, request *metadata_v1.ExamBookStoreServiceCreateRequest) (*metadata_v1.ExamBookStoreServiceCreateResponse, error) {
	meta, _ := metadata.FromIncomingContext(ctx)
	ctx = metadata.NewOutgoingContext(ctx, meta)

	if err := validateQuestionsForSubject(request); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	res, err := s.upstreamClient.Create(ctx, request)
	if err != nil {
		return nil, err
	}

	err = validateMessageWithExamBook(res)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Broken invariant from underlying system: %+v", err)
	}

	return res, nil
}

func (s *ValidationService) Update(ctx context.Context, request *metadata_v1.ExamBookStoreServiceUpdateRequest) (*metadata_v1.ExamBookStoreServiceUpdateResponse, error) {
	meta, _ := metadata.FromIncomingContext(ctx)
	ctx = metadata.NewOutgoingContext(ctx, meta)

	if getRes, err := s.GetGlobal(
		ctx,
		&metadata_v1.ExamBookStoreServiceGetGlobalRequest{GlobalId: request.GetGlobalId()},
	); err == nil {
		book := getRes.GetExamBook()
		book.Questions = request.Questions
		err = validateQuestionsForSubject(book)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
	} else {
		return nil, err
	}

	res, err := s.upstreamClient.Update(ctx, request)
	if err != nil {
		return nil, err
	}

	err = validateMessageWithExamBook(res)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Broken invariant from underlying system: %+v", err)
	}

	return res, nil
}

func (s *ValidationService) Delete(ctx context.Context, request *metadata_v1.ExamBookStoreServiceDeleteRequest) (*metadata_v1.ExamBookStoreServiceDeleteResponse, error) {
	meta, _ := metadata.FromIncomingContext(ctx)
	ctx = metadata.NewOutgoingContext(ctx, meta)

	res, err := s.upstreamClient.Delete(ctx, request)
	if err != nil {
		return nil, err
	}

	err = validateMessageWithExamBook(res)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "Broken invariant from underlying system: %+v", err)
	}

	return res, nil
}

type messageWithExamBook interface {
	GetExamBook() *metadata_v1.ExamBook
}

func validateMessageWithExamBook(message messageWithExamBook) error {
	return validateQuestionsForSubject(message.GetExamBook())
}

type subjectBoundQuestions interface {
	GetSubject() metadata_v1.Subject
	GetQuestions() []*metadata_v1.Question
}

func validateQuestionsForSubject(source subjectBoundQuestions) error {
	switch source.GetSubject() {
	case metadata_v1.Subject_SUBJECT_UNSPECIFIED:
		return errors.New("ExamBook must have a valid subject")

	case metadata_v1.Subject_SUBJECT_LC:
		if len(source.GetQuestions()) != 50 {
			return errors.New("ExamBook of subject SUBJECT_LC must have exactly 50 questions")
		}

	case metadata_v1.Subject_SUBJECT_MT, metadata_v1.Subject_SUBJECT_CH, metadata_v1.Subject_SUBJECT_CN:
		if len(source.GetQuestions()) != 45 {
			return errors.New(fmt.Sprintf(
				"ExamBook of subject %s must have exactly 45 questions",
				source.GetSubject().String(),
			))
		}
	}
	return nil
}
