package service_test

import (
	"context"
	"testing"

	"examBookInvarianceValidator/service"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/enem-data/schemas/go/metadata_v1"
	"gitlab.com/enem-data/schemas/go/mocks/metadata_v1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

func TestNewValidationService(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

	svc := service.NewValidationService(client)

	assert.NotNil(t, svc)
}

var questionsPerSubject = map[metadata_v1.Subject]int{
	metadata_v1.Subject_SUBJECT_LC: 50,
	metadata_v1.Subject_SUBJECT_MT: 45,
	metadata_v1.Subject_SUBJECT_CN: 45,
	metadata_v1.Subject_SUBJECT_CH: 45,
}

func TestValidationService_GetGlobal(t *testing.T) {
	t.Run("malformed response", func(t *testing.T) {
		t.Run("invalid subject", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

			request := &metadata_v1.ExamBookStoreServiceGetGlobalRequest{}
			meta := metadata.New(map[string]string{
				"foo": "bar",
			})
			ctx := metadata.NewIncomingContext(context.Background(), meta)

			client.EXPECT().GetGlobal(
				mock.MatchedBy(func(c context.Context) bool {
					outMeta, _ := metadata.FromOutgoingContext(c)
					return assert.Equal(t, meta, outMeta)
				}),
				gomock.Eq(request),
			).Return(nil, nil)

			svc := service.NewValidationService(client)

			response, err := svc.GetGlobal(ctx, request)
			assert.Nil(t, response)
			assert.ErrorIs(t, err, status.Error(
				codes.Internal,
				"Broken invariant from underlying system: ExamBook must have a valid subject",
			))
		})

		for subject, questions := range questionsPerSubject {
			t.Run("too few questions for "+subject.String(), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				defer ctrl.Finish()

				client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

				request := &metadata_v1.ExamBookStoreServiceGetGlobalRequest{}
				response := &metadata_v1.ExamBookStoreServiceGetGlobalResponse{
					ExamBook: &metadata_v1.ExamBook{
						Subject:   subject,
						Questions: make([]*metadata_v1.Question, questions-1),
					},
				}
				meta := metadata.New(map[string]string{
					"foo": "bar",
				})
				ctx := metadata.NewIncomingContext(context.Background(), meta)

				client.EXPECT().GetGlobal(
					mock.MatchedBy(func(c context.Context) bool {
						outMeta, _ := metadata.FromOutgoingContext(c)
						return assert.Equal(t, meta, outMeta)
					}),
					gomock.Eq(request),
				).Return(response, nil)

				svc := service.NewValidationService(client)

				gotResponse, err := svc.GetGlobal(ctx, request)
				assert.Nil(t, gotResponse)
				assert.ErrorIs(t, err, status.Errorf(
					codes.Internal,
					"Broken invariant from underlying system: ExamBook of subject %s must have exactly %d questions",
					subject.String(),
					questions,
				))
			})
		}

		for subject, questions := range questionsPerSubject {
			t.Run("too many questions for "+subject.String(), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				defer ctrl.Finish()

				client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

				request := &metadata_v1.ExamBookStoreServiceGetGlobalRequest{}
				response := &metadata_v1.ExamBookStoreServiceGetGlobalResponse{
					ExamBook: &metadata_v1.ExamBook{
						Subject:   subject,
						Questions: make([]*metadata_v1.Question, questions+1),
					},
				}
				meta := metadata.New(map[string]string{
					"foo": "bar",
				})
				ctx := metadata.NewIncomingContext(context.Background(), meta)

				client.EXPECT().GetGlobal(
					mock.MatchedBy(func(c context.Context) bool {
						outMeta, _ := metadata.FromOutgoingContext(c)
						return assert.Equal(t, meta, outMeta)
					}),
					gomock.Eq(request),
				).Return(response, nil)

				svc := service.NewValidationService(client)

				gotResponse, err := svc.GetGlobal(ctx, request)
				assert.Nil(t, gotResponse)
				assert.ErrorIs(t, err, status.Errorf(
					codes.Internal,
					"Broken invariant from underlying system: ExamBook of subject %s must have exactly %d questions",
					subject.String(),
					questions,
				))
			})
		}
	})

	for subject, questions := range questionsPerSubject {
		t.Run("forward request for "+subject.String(), func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

			request := &metadata_v1.ExamBookStoreServiceGetGlobalRequest{}
			response := &metadata_v1.ExamBookStoreServiceGetGlobalResponse{
				ExamBook: &metadata_v1.ExamBook{
					Subject:   subject,
					Questions: make([]*metadata_v1.Question, questions),
				},
			}
			meta := metadata.New(map[string]string{
				"foo": "bar",
			})
			ctx := metadata.NewIncomingContext(context.Background(), meta)

			client.EXPECT().GetGlobal(
				mock.MatchedBy(func(c context.Context) bool {
					outMeta, _ := metadata.FromOutgoingContext(c)
					return assert.Equal(t, meta, outMeta)
				}),
				gomock.Eq(request),
			).Return(response, nil)

			svc := service.NewValidationService(client)

			gotResponse, err := svc.GetGlobal(ctx, request)
			assert.Same(t, response, gotResponse)
			assert.NoError(t, err)
		})
	}
}

func TestValidationService_GetLocal(t *testing.T) {
	t.Run("malformed response", func(t *testing.T) {
		t.Run("invalid subject", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

			request := &metadata_v1.ExamBookStoreServiceGetLocalRequest{}
			meta := metadata.New(map[string]string{
				"foo": "bar",
			})
			ctx := metadata.NewIncomingContext(context.Background(), meta)

			client.EXPECT().GetLocal(
				mock.MatchedBy(func(c context.Context) bool {
					outMeta, _ := metadata.FromOutgoingContext(c)
					return assert.Equal(t, meta, outMeta)
				}),
				gomock.Eq(request),
			).Return(nil, nil)

			svc := service.NewValidationService(client)

			response, err := svc.GetLocal(ctx, request)
			assert.Nil(t, response)
			assert.ErrorIs(t, err, status.Error(
				codes.Internal,
				"Broken invariant from underlying system: ExamBook must have a valid subject",
			))
		})

		for subject, questions := range questionsPerSubject {
			t.Run("too few questions for "+subject.String(), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				defer ctrl.Finish()

				client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

				request := &metadata_v1.ExamBookStoreServiceGetLocalRequest{}
				response := &metadata_v1.ExamBookStoreServiceGetLocalResponse{
					ExamBook: &metadata_v1.ExamBook{
						Subject:   subject,
						Questions: make([]*metadata_v1.Question, questions-1),
					},
				}
				meta := metadata.New(map[string]string{
					"foo": "bar",
				})
				ctx := metadata.NewIncomingContext(context.Background(), meta)

				client.EXPECT().GetLocal(
					mock.MatchedBy(func(c context.Context) bool {
						outMeta, _ := metadata.FromOutgoingContext(c)
						return assert.Equal(t, meta, outMeta)
					}),
					gomock.Eq(request),
				).Return(response, nil)

				svc := service.NewValidationService(client)

				gotResponse, err := svc.GetLocal(ctx, request)
				assert.Nil(t, gotResponse)
				assert.ErrorIs(t, err, status.Errorf(
					codes.Internal,
					"Broken invariant from underlying system: ExamBook of subject %s must have exactly %d questions",
					subject.String(),
					questions,
				))
			})
		}

		for subject, questions := range questionsPerSubject {
			t.Run("too many questions for "+subject.String(), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				defer ctrl.Finish()

				client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

				request := &metadata_v1.ExamBookStoreServiceGetLocalRequest{}
				response := &metadata_v1.ExamBookStoreServiceGetLocalResponse{
					ExamBook: &metadata_v1.ExamBook{
						Subject:   subject,
						Questions: make([]*metadata_v1.Question, questions+1),
					},
				}
				meta := metadata.New(map[string]string{
					"foo": "bar",
				})
				ctx := metadata.NewIncomingContext(context.Background(), meta)

				client.EXPECT().GetLocal(
					mock.MatchedBy(func(c context.Context) bool {
						outMeta, _ := metadata.FromOutgoingContext(c)
						return assert.Equal(t, meta, outMeta)
					}),
					gomock.Eq(request),
				).Return(response, nil)

				svc := service.NewValidationService(client)

				gotResponse, err := svc.GetLocal(ctx, request)
				assert.Nil(t, gotResponse)
				assert.ErrorIs(t, err, status.Errorf(
					codes.Internal,
					"Broken invariant from underlying system: ExamBook of subject %s must have exactly %d questions",
					subject.String(),
					questions,
				))
			})
		}
	})

	for subject, questions := range questionsPerSubject {
		t.Run("forward request for "+subject.String(), func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

			request := &metadata_v1.ExamBookStoreServiceGetLocalRequest{}
			response := &metadata_v1.ExamBookStoreServiceGetLocalResponse{
				ExamBook: &metadata_v1.ExamBook{
					Subject:   subject,
					Questions: make([]*metadata_v1.Question, questions),
				},
			}
			meta := metadata.New(map[string]string{
				"foo": "bar",
			})
			ctx := metadata.NewIncomingContext(context.Background(), meta)

			client.EXPECT().GetLocal(
				mock.MatchedBy(func(c context.Context) bool {
					outMeta, _ := metadata.FromOutgoingContext(c)
					return assert.Equal(t, meta, outMeta)
				}),
				gomock.Eq(request),
			).Return(response, nil)

			svc := service.NewValidationService(client)

			gotResponse, err := svc.GetLocal(ctx, request)
			assert.Same(t, response, gotResponse)
			assert.NoError(t, err)
		})
	}
}

func TestValidationService_Create(t *testing.T) {
	t.Run("malformed request", func(t *testing.T) {
		t.Run("invalid subject", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)
			svc := service.NewValidationService(client)

			request := &metadata_v1.ExamBookStoreServiceCreateRequest{}

			response, err := svc.Create(context.Background(), request)
			assert.Nil(t, response)
			assert.ErrorIs(t, err, status.Error(codes.InvalidArgument, "ExamBook must have a valid subject"))
		})

		for subject, questions := range questionsPerSubject {
			t.Run("too few questions for "+subject.String(), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				defer ctrl.Finish()

				client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)
				svc := service.NewValidationService(client)

				request := &metadata_v1.ExamBookStoreServiceCreateRequest{
					Subject:   subject,
					Questions: make([]*metadata_v1.Question, questions-1),
				}

				response, err := svc.Create(context.Background(), request)
				assert.Nil(t, response)
				assert.ErrorIs(t, err, status.Errorf(
					codes.InvalidArgument,
					"ExamBook of subject %s must have exactly %d questions",
					subject.String(),
					questions,
				))
			})
		}

		for subject, questions := range questionsPerSubject {
			t.Run("too many questions for "+subject.String(), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				defer ctrl.Finish()

				client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)
				svc := service.NewValidationService(client)

				request := &metadata_v1.ExamBookStoreServiceCreateRequest{
					Subject:   subject,
					Questions: make([]*metadata_v1.Question, questions+1),
				}

				response, err := svc.Create(context.Background(), request)
				assert.Nil(t, response)
				assert.ErrorIs(t, err, status.Errorf(
					codes.InvalidArgument,
					"ExamBook of subject %s must have exactly %d questions",
					subject.String(),
					questions,
				))
			})
		}
	})

	t.Run("malformed response", func(t *testing.T) {
		t.Run("invalid subject", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

			request := &metadata_v1.ExamBookStoreServiceCreateRequest{
				Subject:   metadata_v1.Subject_SUBJECT_CN,
				Questions: make([]*metadata_v1.Question, 45),
			}
			meta := metadata.New(map[string]string{
				"foo": "bar",
			})
			ctx := metadata.NewIncomingContext(context.Background(), meta)

			client.EXPECT().Create(
				mock.MatchedBy(func(c context.Context) bool {
					outMeta, _ := metadata.FromOutgoingContext(c)
					return assert.Equal(t, meta, outMeta)
				}),
				gomock.Eq(request),
			).Return(nil, nil)

			svc := service.NewValidationService(client)

			response, err := svc.Create(ctx, request)
			assert.Nil(t, response)
			assert.ErrorIs(t, err, status.Error(
				codes.Internal,
				"Broken invariant from underlying system: ExamBook must have a valid subject",
			))
		})

		for subject, questions := range questionsPerSubject {
			t.Run("too few questions for "+subject.String(), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				defer ctrl.Finish()

				client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

				request := &metadata_v1.ExamBookStoreServiceCreateRequest{
					Subject:   subject,
					Questions: make([]*metadata_v1.Question, questions),
				}
				response := &metadata_v1.ExamBookStoreServiceCreateResponse{
					ExamBook: &metadata_v1.ExamBook{
						Subject:   subject,
						Questions: make([]*metadata_v1.Question, questions-1),
					},
				}
				meta := metadata.New(map[string]string{
					"foo": "bar",
				})
				ctx := metadata.NewIncomingContext(context.Background(), meta)

				client.EXPECT().Create(
					mock.MatchedBy(func(c context.Context) bool {
						outMeta, _ := metadata.FromOutgoingContext(c)
						return assert.Equal(t, meta, outMeta)
					}),
					gomock.Eq(request),
				).Return(response, nil)

				svc := service.NewValidationService(client)

				gotResponse, err := svc.Create(ctx, request)
				assert.Nil(t, gotResponse)
				assert.ErrorIs(t, err, status.Errorf(
					codes.Internal,
					"Broken invariant from underlying system: ExamBook of subject %s must have exactly %d questions",
					subject.String(),
					questions,
				))
			})
		}

		for subject, questions := range questionsPerSubject {
			t.Run("too many questions for "+subject.String(), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				defer ctrl.Finish()

				client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

				request := &metadata_v1.ExamBookStoreServiceCreateRequest{
					Subject:   subject,
					Questions: make([]*metadata_v1.Question, questions),
				}
				response := &metadata_v1.ExamBookStoreServiceCreateResponse{
					ExamBook: &metadata_v1.ExamBook{
						Subject:   subject,
						Questions: make([]*metadata_v1.Question, questions+1),
					},
				}
				meta := metadata.New(map[string]string{
					"foo": "bar",
				})
				ctx := metadata.NewIncomingContext(context.Background(), meta)

				client.EXPECT().Create(
					mock.MatchedBy(func(c context.Context) bool {
						outMeta, _ := metadata.FromOutgoingContext(c)
						return assert.Equal(t, meta, outMeta)
					}),
					gomock.Eq(request),
				).Return(response, nil)

				svc := service.NewValidationService(client)

				gotResponse, err := svc.Create(ctx, request)
				assert.Nil(t, gotResponse)
				assert.ErrorIs(t, err, status.Errorf(
					codes.Internal,
					"Broken invariant from underlying system: ExamBook of subject %s must have exactly %d questions",
					subject.String(),
					questions,
				))
			})
		}
	})

	for subject, questions := range questionsPerSubject {
		t.Run("forward request for "+subject.String(), func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

			request := &metadata_v1.ExamBookStoreServiceCreateRequest{
				Subject:   subject,
				Questions: make([]*metadata_v1.Question, questions),
			}
			response := &metadata_v1.ExamBookStoreServiceCreateResponse{
				ExamBook: &metadata_v1.ExamBook{
					Subject:   subject,
					Questions: make([]*metadata_v1.Question, questions),
				},
			}
			meta := metadata.New(map[string]string{
				"foo": "bar",
			})
			ctx := metadata.NewIncomingContext(context.Background(), meta)

			client.EXPECT().Create(
				mock.MatchedBy(func(c context.Context) bool {
					outMeta, _ := metadata.FromOutgoingContext(c)
					return assert.Equal(t, meta, outMeta)
				}),
				gomock.Eq(request),
			).Return(response, nil)

			svc := service.NewValidationService(client)

			gotResponse, err := svc.Create(ctx, request)
			assert.Same(t, response, gotResponse)
			assert.NoError(t, err)
		})
	}
}

func TestValidationService_Update(t *testing.T) {
	t.Run("malformed request", func(t *testing.T) {
		for subject, questions := range questionsPerSubject {
			t.Run("too few questions for "+subject.String(), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				defer ctrl.Finish()

				client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)
				svc := service.NewValidationService(client)

				request := &metadata_v1.ExamBookStoreServiceUpdateRequest{
					GlobalId:  1234,
					Questions: make([]*metadata_v1.Question, questions-1),
				}
				meta := metadata.New(map[string]string{
					"foo": "bar",
				})
				ctx := metadata.NewIncomingContext(context.Background(), meta)

				getRequest := &metadata_v1.ExamBookStoreServiceGetGlobalRequest{
					GlobalId: 1234,
				}
				getResponse := &metadata_v1.ExamBookStoreServiceGetGlobalResponse{
					ExamBook: &metadata_v1.ExamBook{
						Subject:   subject,
						Questions: make([]*metadata_v1.Question, questions),
					},
				}

				client.EXPECT().GetGlobal(
					mock.MatchedBy(func(c context.Context) bool {
						outMeta, _ := metadata.FromOutgoingContext(c)
						return assert.Equal(t, meta, outMeta)
					}),
					gomock.Eq(getRequest),
				).Return(getResponse, nil)

				response, err := svc.Update(ctx, request)
				assert.Nil(t, response)
				assert.ErrorIs(t, err, status.Errorf(
					codes.InvalidArgument,
					"ExamBook of subject %s must have exactly %d questions",
					subject.String(),
					questions,
				))
			})
		}

		for subject, questions := range questionsPerSubject {
			t.Run("too many questions for "+subject.String(), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				defer ctrl.Finish()

				client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)
				svc := service.NewValidationService(client)

				request := &metadata_v1.ExamBookStoreServiceUpdateRequest{
					GlobalId:  1234,
					Questions: make([]*metadata_v1.Question, questions-1),
				}
				meta := metadata.New(map[string]string{
					"foo": "bar",
				})
				ctx := metadata.NewIncomingContext(context.Background(), meta)

				getRequest := &metadata_v1.ExamBookStoreServiceGetGlobalRequest{
					GlobalId: 1234,
				}
				getResponse := &metadata_v1.ExamBookStoreServiceGetGlobalResponse{
					ExamBook: &metadata_v1.ExamBook{
						Subject:   subject,
						Questions: make([]*metadata_v1.Question, questions),
					},
				}

				client.EXPECT().GetGlobal(
					mock.MatchedBy(func(c context.Context) bool {
						outMeta, _ := metadata.FromOutgoingContext(c)
						return assert.Equal(t, meta, outMeta)
					}),
					gomock.Eq(getRequest),
				).Return(getResponse, nil)

				response, err := svc.Update(ctx, request)
				assert.Nil(t, response)
				assert.ErrorIs(t, err, status.Errorf(
					codes.InvalidArgument,
					"ExamBook of subject %s must have exactly %d questions",
					subject.String(),
					questions,
				))
			})
		}
	})

	t.Run("malformed response", func(t *testing.T) {
		t.Run("invalid subject", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

			request := &metadata_v1.ExamBookStoreServiceUpdateRequest{
				GlobalId:  1234,
				Questions: make([]*metadata_v1.Question, 45),
			}
			meta := metadata.New(map[string]string{
				"foo": "bar",
			})
			ctx := metadata.NewIncomingContext(context.Background(), meta)

			getRequest := &metadata_v1.ExamBookStoreServiceGetGlobalRequest{
				GlobalId: 1234,
			}
			getResponse := &metadata_v1.ExamBookStoreServiceGetGlobalResponse{}

			client.EXPECT().GetGlobal(
				mock.MatchedBy(func(c context.Context) bool {
					outMeta, _ := metadata.FromOutgoingContext(c)
					return assert.Equal(t, meta, outMeta)
				}),
				gomock.Eq(getRequest),
			).Return(getResponse, nil)

			svc := service.NewValidationService(client)

			response, err := svc.Update(ctx, request)
			assert.Nil(t, response)
			assert.ErrorIs(t, err, status.Error(
				codes.Internal,
				"Broken invariant from underlying system: ExamBook must have a valid subject",
			))
		})

		for subject, questions := range questionsPerSubject {
			t.Run("too few questions for "+subject.String(), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				defer ctrl.Finish()

				client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

				request := &metadata_v1.ExamBookStoreServiceUpdateRequest{
					GlobalId:  1234,
					Questions: make([]*metadata_v1.Question, questions),
				}
				response := &metadata_v1.ExamBookStoreServiceUpdateResponse{
					ExamBook: &metadata_v1.ExamBook{
						Subject:   subject,
						Questions: make([]*metadata_v1.Question, questions-1),
					},
				}
				meta := metadata.New(map[string]string{
					"foo": "bar",
				})
				ctx := metadata.NewIncomingContext(context.Background(), meta)

				getRequest := &metadata_v1.ExamBookStoreServiceGetGlobalRequest{
					GlobalId: 1234,
				}
				getResponse := &metadata_v1.ExamBookStoreServiceGetGlobalResponse{
					ExamBook: &metadata_v1.ExamBook{
						Subject:   subject,
						Questions: make([]*metadata_v1.Question, questions),
					},
				}

				client.EXPECT().GetGlobal(
					mock.MatchedBy(func(c context.Context) bool {
						outMeta, _ := metadata.FromOutgoingContext(c)
						return assert.Equal(t, meta, outMeta)
					}),
					gomock.Eq(getRequest),
				).Return(getResponse, nil)

				client.EXPECT().Update(
					mock.MatchedBy(func(c context.Context) bool {
						outMeta, _ := metadata.FromOutgoingContext(c)
						return assert.Equal(t, meta, outMeta)
					}),
					gomock.Eq(request),
				).Return(response, nil)

				svc := service.NewValidationService(client)

				gotResponse, err := svc.Update(ctx, request)
				assert.Nil(t, gotResponse)
				assert.ErrorIs(t, err, status.Errorf(
					codes.Internal,
					"Broken invariant from underlying system: ExamBook of subject %s must have exactly %d questions",
					subject.String(),
					questions,
				))
			})
		}

		for subject, questions := range questionsPerSubject {
			t.Run("too many questions for "+subject.String(), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				defer ctrl.Finish()

				client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

				request := &metadata_v1.ExamBookStoreServiceUpdateRequest{
					GlobalId:  1234,
					Questions: make([]*metadata_v1.Question, questions),
				}
				response := &metadata_v1.ExamBookStoreServiceUpdateResponse{
					ExamBook: &metadata_v1.ExamBook{
						Subject:   subject,
						Questions: make([]*metadata_v1.Question, questions+1),
					},
				}
				meta := metadata.New(map[string]string{
					"foo": "bar",
				})
				ctx := metadata.NewIncomingContext(context.Background(), meta)

				getRequest := &metadata_v1.ExamBookStoreServiceGetGlobalRequest{
					GlobalId: 1234,
				}
				getResponse := &metadata_v1.ExamBookStoreServiceGetGlobalResponse{
					ExamBook: &metadata_v1.ExamBook{
						Subject:   subject,
						Questions: make([]*metadata_v1.Question, questions),
					},
				}

				client.EXPECT().GetGlobal(
					mock.MatchedBy(func(c context.Context) bool {
						outMeta, _ := metadata.FromOutgoingContext(c)
						return assert.Equal(t, meta, outMeta)
					}),
					gomock.Eq(getRequest),
				).Return(getResponse, nil)

				client.EXPECT().Update(
					mock.MatchedBy(func(c context.Context) bool {
						outMeta, _ := metadata.FromOutgoingContext(c)
						return assert.Equal(t, meta, outMeta)
					}),
					gomock.Eq(request),
				).Return(response, nil)

				svc := service.NewValidationService(client)

				gotResponse, err := svc.Update(ctx, request)
				assert.Nil(t, gotResponse)
				assert.ErrorIs(t, err, status.Errorf(
					codes.Internal,
					"Broken invariant from underlying system: ExamBook of subject %s must have exactly %d questions",
					subject.String(),
					questions,
				))
			})
		}
	})

	for subject, questions := range questionsPerSubject {
		t.Run("forward request for "+subject.String(), func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

			request := &metadata_v1.ExamBookStoreServiceUpdateRequest{
				GlobalId:  1234,
				Questions: make([]*metadata_v1.Question, questions),
			}
			response := &metadata_v1.ExamBookStoreServiceUpdateResponse{
				ExamBook: &metadata_v1.ExamBook{
					Subject:   subject,
					Questions: make([]*metadata_v1.Question, questions),
				},
			}
			meta := metadata.New(map[string]string{
				"foo": "bar",
			})
			ctx := metadata.NewIncomingContext(context.Background(), meta)

			getRequest := &metadata_v1.ExamBookStoreServiceGetGlobalRequest{
				GlobalId: 1234,
			}
			getResponse := &metadata_v1.ExamBookStoreServiceGetGlobalResponse{
				ExamBook: &metadata_v1.ExamBook{
					Subject:   subject,
					Questions: make([]*metadata_v1.Question, questions),
				},
			}

			client.EXPECT().GetGlobal(
				mock.MatchedBy(func(c context.Context) bool {
					outMeta, _ := metadata.FromOutgoingContext(c)
					return assert.Equal(t, meta, outMeta)
				}),
				gomock.Eq(getRequest),
			).Return(getResponse, nil)

			client.EXPECT().Update(
				mock.MatchedBy(func(c context.Context) bool {
					outMeta, _ := metadata.FromOutgoingContext(c)
					return assert.Equal(t, meta, outMeta)
				}),
				gomock.Eq(request),
			).Return(response, nil)

			svc := service.NewValidationService(client)

			gotResponse, err := svc.Update(ctx, request)
			assert.Same(t, response, gotResponse)
			assert.NoError(t, err)
		})
	}
}

func TestValidationService_Delete(t *testing.T) {
	t.Run("malformed response", func(t *testing.T) {
		t.Run("invalid subject", func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

			request := &metadata_v1.ExamBookStoreServiceDeleteRequest{}
			meta := metadata.New(map[string]string{
				"foo": "bar",
			})
			ctx := metadata.NewIncomingContext(context.Background(), meta)

			client.EXPECT().Delete(
				mock.MatchedBy(func(c context.Context) bool {
					outMeta, _ := metadata.FromOutgoingContext(c)
					return assert.Equal(t, meta, outMeta)
				}),
				gomock.Eq(request),
			).Return(nil, nil)

			svc := service.NewValidationService(client)

			response, err := svc.Delete(ctx, request)
			assert.Nil(t, response)
			assert.ErrorIs(t, err, status.Error(
				codes.Internal,
				"Broken invariant from underlying system: ExamBook must have a valid subject",
			))
		})

		for subject, questions := range questionsPerSubject {
			t.Run("too few questions for "+subject.String(), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				defer ctrl.Finish()

				client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

				request := &metadata_v1.ExamBookStoreServiceDeleteRequest{}
				response := &metadata_v1.ExamBookStoreServiceDeleteResponse{
					ExamBook: &metadata_v1.ExamBook{
						Subject:   subject,
						Questions: make([]*metadata_v1.Question, questions-1),
					},
				}
				meta := metadata.New(map[string]string{
					"foo": "bar",
				})
				ctx := metadata.NewIncomingContext(context.Background(), meta)

				client.EXPECT().Delete(
					mock.MatchedBy(func(c context.Context) bool {
						outMeta, _ := metadata.FromOutgoingContext(c)
						return assert.Equal(t, meta, outMeta)
					}),
					gomock.Eq(request),
				).Return(response, nil)

				svc := service.NewValidationService(client)

				gotResponse, err := svc.Delete(ctx, request)
				assert.Nil(t, gotResponse)
				assert.ErrorIs(t, err, status.Errorf(
					codes.Internal,
					"Broken invariant from underlying system: ExamBook of subject %s must have exactly %d questions",
					subject.String(),
					questions,
				))
			})
		}

		for subject, questions := range questionsPerSubject {
			t.Run("too many questions for "+subject.String(), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				defer ctrl.Finish()

				client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

				request := &metadata_v1.ExamBookStoreServiceDeleteRequest{}
				response := &metadata_v1.ExamBookStoreServiceDeleteResponse{
					ExamBook: &metadata_v1.ExamBook{
						Subject:   subject,
						Questions: make([]*metadata_v1.Question, questions+1),
					},
				}
				meta := metadata.New(map[string]string{
					"foo": "bar",
				})
				ctx := metadata.NewIncomingContext(context.Background(), meta)

				client.EXPECT().Delete(
					mock.MatchedBy(func(c context.Context) bool {
						outMeta, _ := metadata.FromOutgoingContext(c)
						return assert.Equal(t, meta, outMeta)
					}),
					gomock.Eq(request),
				).Return(response, nil)

				svc := service.NewValidationService(client)

				gotResponse, err := svc.Delete(ctx, request)
				assert.Nil(t, gotResponse)
				assert.ErrorIs(t, err, status.Errorf(
					codes.Internal,
					"Broken invariant from underlying system: ExamBook of subject %s must have exactly %d questions",
					subject.String(),
					questions,
				))
			})
		}
	})

	for subject, questions := range questionsPerSubject {
		t.Run("forward request for "+subject.String(), func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			client := mock_metadata_v1.NewMockExamBookStoreServiceClient(ctrl)

			request := &metadata_v1.ExamBookStoreServiceDeleteRequest{}
			response := &metadata_v1.ExamBookStoreServiceDeleteResponse{
				ExamBook: &metadata_v1.ExamBook{
					Subject:   subject,
					Questions: make([]*metadata_v1.Question, questions),
				},
			}
			meta := metadata.New(map[string]string{
				"foo": "bar",
			})
			ctx := metadata.NewIncomingContext(context.Background(), meta)

			client.EXPECT().Delete(
				mock.MatchedBy(func(c context.Context) bool {
					outMeta, _ := metadata.FromOutgoingContext(c)
					return assert.Equal(t, meta, outMeta)
				}),
				gomock.Eq(request),
			).Return(response, nil)

			svc := service.NewValidationService(client)

			gotResponse, err := svc.Delete(ctx, request)
			assert.Same(t, response, gotResponse)
			assert.NoError(t, err)
		})
	}
}
