package cmd

import (
	"context"
	"log"
	"net"
	"os"
	"os/signal"
	"time"

	"examBookInvarianceValidator/service"
	"gitlab.com/enem-data/schemas/go/metadata_v1"
	"google.golang.org/grpc"
)

func startService(config configuration) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	dialOpts := []grpc.DialOption{grpc.WithBlock()}

	if config.InsecureUpstream {
		dialOpts = append(dialOpts, grpc.WithInsecure())
	}

	conn, err := grpc.DialContext(ctx, config.Upstream, dialOpts...)
	checkErr(err)

	log.Println("Connected to upstream client")

	client := metadata_v1.NewExamBookStoreServiceClient(conn)
	svc := service.NewValidationService(client)

	server := grpc.NewServer()
	metadata_v1.RegisterExamBookStoreServiceServer(server, svc)

	log.Printf("Listening on %q", config.Bind)
	lis, err := net.Listen("tcp", config.Bind)
	checkErr(err)
	defer func(lis net.Listener) {
		checkErr(lis.Close())
	}(lis)

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)

	go func() {
		defer stop()
		checkErr(server.Serve(lis))
	}()

	<-ctx.Done()
	server.GracefulStop()
	stop()
}

func checkErr(err error) {
	if err != nil {
		log.Panicf("Error: %+v", err)
	}
}
