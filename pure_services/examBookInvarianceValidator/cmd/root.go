package cmd

import (
	"fmt"
	"os"

	"github.com/mitchellh/mapstructure"
	"github.com/spf13/cobra"

	"github.com/spf13/viper"
)

type configuration struct {
	Upstream string
	Bind     string

	InsecureUpstream bool `viper:"insecure_upstream"`
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "examBookInvarianceValidator",
	Short: "GRPC ExamBookStore middleware to ensure question order invariance",
	PreRunE: func(cmd *cobra.Command, args []string) error {
		return viper.BindPFlags(cmd.Flags())
	},
	Run: func(cmd *cobra.Command, args []string) {
		config := configuration{}
		cobra.CheckErr(viper.Unmarshal(&config, ViperDecoderTagName("viper")))

		startService(config)
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

var cfgFile string

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.examBookInvarianceValidator.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().String("upstream", "", "upstream service link")
	cobra.CheckErr(rootCmd.MarkFlagRequired("upstream"))
	rootCmd.Flags().String("bind", "localhost:4000", "bind address for service")
	rootCmd.Flags().Bool("insecure_upstream", false, "synchronously connect to upstream service")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".examBookInvarianceValidator" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".examBookInvarianceValidator")
	}

	viper.SetEnvPrefix("ENEM")
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		_, err = fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
		cobra.CheckErr(err)
	}
}

func ViperDecoderTagName(tagName string) viper.DecoderConfigOption {
	return func(config *mapstructure.DecoderConfig) {
		config.TagName = tagName
	}
}
