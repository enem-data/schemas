use std::collections::HashMap;
use std::convert::Infallible;
use std::fs;
use std::io;
use std::iter::FromIterator;
use std::path::{Path, PathBuf};

use walkdir::WalkDir;

fn compile_proto_files(
    proto_dir: &impl AsRef<Path>,
    target_dir: &impl AsRef<Path>,
) -> Result<(), Box<dyn std::error::Error>> {
    let proto_files: Vec<_> = WalkDir::new(proto_dir)
        .into_iter()
        .filter_map(|r| r.ok())
        .filter(|d| {
            d.file_type().is_file()
                && d.path().extension().and_then(|os| os.to_str()) == Some("proto")
        })
        .map(|f| f.into_path())
        .collect();

    fs::remove_dir_all(target_dir)?;
    fs::create_dir_all(target_dir)?;

    tonic_build::configure()
        .out_dir(target_dir)
        .compile_well_known_types(true)
        .compile(&proto_files, &[proto_dir.as_ref().into()])?;

    Ok(())
}

#[derive(Debug)]
struct ModTree {
    content: Option<String>,
    sub_mods: HashMap<String, Box<ModTree>>,
}

impl ModTree {
    fn new() -> Self {
        Self {
            content: None,
            sub_mods: HashMap::new(),
        }
    }

    fn new_boxed() -> Box<Self> {
        Box::new(Self::new())
    }

    fn add_proto_mod(&mut self, proto_mod: String) {
        let mut tree = self;
        let parts: Vec<_> = proto_mod.split('.').collect();

        for component in parts {
            tree = tree
                .sub_mods
                .entry(String::from(component))
                .or_insert_with(ModTree::new_boxed);
        }

        tree.content = Some(proto_mod);
    }

    fn write_to(&self, base_dir: &Path, target_dir: &Path) -> Result<(), Infallible> {
        fs::create_dir_all(target_dir).unwrap();
        let mut out_mod = fs::File::create(target_dir.join("mod.rs")).unwrap();

        for (name, content) in self.sub_mods.iter() {
            io::Write::write_fmt(&mut out_mod, format_args!("pub mod {};\n", name)).unwrap();
            content.write_to(base_dir, &target_dir.join(name))?;
        }

        if let Some(mod_name) = &self.content {
            let mod_path = base_dir.join(format!("{}.rs", mod_name));

            let mut in_mod = fs::File::open(mod_path).unwrap();

            io::copy(&mut in_mod, &mut out_mod).unwrap();
        }

        Ok(())
    }
}

impl FromIterator<String> for ModTree {
    fn from_iter<T: IntoIterator<Item = String>>(iter: T) -> Self {
        let mut tree = Self::new();

        iter.into_iter().for_each(|s| tree.add_proto_mod(s));

        tree
    }
}

fn generate_mod_file(dir: &impl AsRef<Path>, target: &impl AsRef<Path>) -> io::Result<()> {
    let dir = dir.as_ref();
    let target = target.as_ref();

    let proto_mods: ModTree = fs::read_dir(dir)?
        .into_iter()
        .filter_map(|r| r.ok())
        .map(|d| {
            d.path()
                .file_stem()
                .expect("the target folder should only have rust files with valid names")
                .to_os_string()
                .into_string()
                .expect("all the names must be valid unicode sequences")
        })
        .collect();

    proto_mods
        .write_to(dir, target)
        .expect("failed to create mod file");

    tonic_build::fmt(target.to_str().expect("dir_name should be a valid str"));

    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let proto_files = PathBuf::from("../proto");
    let build_folder = PathBuf::from(std::env::var_os("OUT_DIR").unwrap());
    let target = PathBuf::from("src/proto");

    compile_proto_files(&proto_files, &build_folder)?;
    generate_mod_file(&build_folder, &target)?;

    Ok(())
}
