#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Competence {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
    #[prost(enumeration = "Subject", tag = "2")]
    pub subject: i32,
    #[prost(uint32, tag = "3")]
    pub local_id: u32,
    /// https://download.inep.gov.br/download/enem/matriz_referencia.pdf
    #[prost(string, optional, tag = "4")]
    pub description: ::core::option::Option<::prost::alloc::string::String>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Skill {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
    #[prost(uint32, tag = "2")]
    pub competence_id: u32,
    #[prost(uint32, tag = "3")]
    pub local_id: u32,
    /// https://download.inep.gov.br/download/enem/matriz_referencia.pdf
    #[prost(string, optional, tag = "4")]
    pub description: ::core::option::Option<::prost::alloc::string::String>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Question {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
    #[prost(uint32, tag = "2")]
    pub year: u32,
    #[prost(enumeration = "Subject", tag = "3")]
    pub subject_id: i32,
    #[prost(uint32, optional, tag = "4")]
    pub skill_id: ::core::option::Option<u32>,
    #[prost(enumeration = "QuestionAnswer", tag = "5")]
    pub answer: i32,
    /// Indicates whether the answer of this question was successfully disputed by the community.
    /// Disputed questions are not included in searches by competence or skill since they have failed their initial design.
    #[prost(bool, tag = "6")]
    pub disputed: bool,
    /// Indicates whether this questions was presented as part of the adapted ExamBook.
    /// Questions from the adapted ExamBook are considered separately for statistics.
    #[prost(bool, tag = "7")]
    pub adapted_exam: bool,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ExamBook {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
    #[prost(uint32, tag = "2")]
    pub year: u32,
    /// The INEP ID for this particular ExamBook. These are unique within the same year.
    #[prost(uint32, tag = "3")]
    pub local_id: u32,
    /// The color of the ExamBook, used to identify the ordering of the questions by the participants.
    /// Here the value is normalized to full lower case.
    #[prost(string, tag = "4")]
    pub color: ::prost::alloc::string::String,
    #[prost(enumeration = "Subject", tag = "5")]
    pub subject: i32,
    /// An ExamBook of SUBJECT_LC will always have 50 questions in this order:
    ///  - English specific questions, numbered 1 - 5
    ///  - Spanish specific questions, numbered 1 - 5
    ///  - Native language questions, numbered 6 - 45
    ///
    /// ExamBooks of any other subject will always have exactly 45 questions.
    #[prost(message, repeated, tag = "6")]
    pub questions: ::prost::alloc::vec::Vec<Question>,
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
#[repr(i32)]
pub enum Subject {
    Unspecified = 0,
    Lc = 1,
    Mt = 2,
    Ch = 3,
    Cn = 4,
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
#[repr(i32)]
pub enum QuestionAnswer {
    Unspecified = 0,
    A = 1,
    B = 2,
    C = 3,
    D = 4,
    E = 5,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestionStoreServiceGetGlobalRequest {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestionStoreServiceGetGlobalResponse {
    #[prost(message, optional, tag = "1")]
    pub question: ::core::option::Option<Question>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestionStoreServiceGetLocalResponse {
    #[prost(message, optional, tag = "1")]
    pub question: ::core::option::Option<Question>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestionStoreServiceSaveRequest {
    #[prost(message, optional, tag = "1")]
    pub question: ::core::option::Option<Question>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestionStoreServiceSaveResponse {
    #[prost(message, optional, tag = "1")]
    pub question: ::core::option::Option<Question>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestionStoreServiceDeleteRequest {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestionStoreServiceDeleteResponse {
    #[prost(message, optional, tag = "1")]
    pub question: ::core::option::Option<Question>,
}
#[doc = r" Generated client implementations."]
pub mod question_store_service_client {
    #![allow(unused_variables, dead_code, missing_docs, clippy::let_unit_value)]
    use tonic::codegen::*;
    #[derive(Debug, Clone)]
    pub struct QuestionStoreServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl QuestionStoreServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> QuestionStoreServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + Send + Sync + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as Body>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor<F>(
            inner: T,
            interceptor: F,
        ) -> QuestionStoreServiceClient<InterceptedService<T, F>>
        where
            F: tonic::service::Interceptor,
            T: tonic::codegen::Service<
                http::Request<tonic::body::BoxBody>,
                Response = http::Response<
                    <T as tonic::client::GrpcService<tonic::body::BoxBody>>::ResponseBody,
                >,
            >,
            <T as tonic::codegen::Service<http::Request<tonic::body::BoxBody>>>::Error:
                Into<StdError> + Send + Sync,
        {
            QuestionStoreServiceClient::new(InterceptedService::new(inner, interceptor))
        }
        #[doc = r" Compress requests with `gzip`."]
        #[doc = r""]
        #[doc = r" This requires the server to support it otherwise it might respond with an"]
        #[doc = r" error."]
        pub fn send_gzip(mut self) -> Self {
            self.inner = self.inner.send_gzip();
            self
        }
        #[doc = r" Enable decompressing responses with `gzip`."]
        pub fn accept_gzip(mut self) -> Self {
            self.inner = self.inner.accept_gzip();
            self
        }
        pub async fn get_global(
            &mut self,
            request: impl tonic::IntoRequest<super::QuestionStoreServiceGetGlobalRequest>,
        ) -> Result<tonic::Response<super::QuestionStoreServiceGetGlobalResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/enem.metadata.v1.QuestionStoreService/GetGlobal",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn save(
            &mut self,
            request: impl tonic::IntoRequest<super::QuestionStoreServiceSaveRequest>,
        ) -> Result<tonic::Response<super::QuestionStoreServiceSaveResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/enem.metadata.v1.QuestionStoreService/Save");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn delete(
            &mut self,
            request: impl tonic::IntoRequest<super::QuestionStoreServiceDeleteRequest>,
        ) -> Result<tonic::Response<super::QuestionStoreServiceDeleteResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/enem.metadata.v1.QuestionStoreService/Delete",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod question_store_service_server {
    #![allow(unused_variables, dead_code, missing_docs, clippy::let_unit_value)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with QuestionStoreServiceServer."]
    #[async_trait]
    pub trait QuestionStoreService: Send + Sync + 'static {
        async fn get_global(
            &self,
            request: tonic::Request<super::QuestionStoreServiceGetGlobalRequest>,
        ) -> Result<tonic::Response<super::QuestionStoreServiceGetGlobalResponse>, tonic::Status>;
        async fn save(
            &self,
            request: tonic::Request<super::QuestionStoreServiceSaveRequest>,
        ) -> Result<tonic::Response<super::QuestionStoreServiceSaveResponse>, tonic::Status>;
        async fn delete(
            &self,
            request: tonic::Request<super::QuestionStoreServiceDeleteRequest>,
        ) -> Result<tonic::Response<super::QuestionStoreServiceDeleteResponse>, tonic::Status>;
    }
    #[derive(Debug)]
    pub struct QuestionStoreServiceServer<T: QuestionStoreService> {
        inner: _Inner<T>,
        accept_compression_encodings: (),
        send_compression_encodings: (),
    }
    struct _Inner<T>(Arc<T>);
    impl<T: QuestionStoreService> QuestionStoreServiceServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner);
            Self {
                inner,
                accept_compression_encodings: Default::default(),
                send_compression_encodings: Default::default(),
            }
        }
        pub fn with_interceptor<F>(inner: T, interceptor: F) -> InterceptedService<Self, F>
        where
            F: tonic::service::Interceptor,
        {
            InterceptedService::new(Self::new(inner), interceptor)
        }
    }
    impl<T, B> tonic::codegen::Service<http::Request<B>> for QuestionStoreServiceServer<T>
    where
        T: QuestionStoreService,
        B: Body + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/enem.metadata.v1.QuestionStoreService/GetGlobal" => {
                    #[allow(non_camel_case_types)]
                    struct GetGlobalSvc<T: QuestionStoreService>(pub Arc<T>);
                    impl<T: QuestionStoreService>
                        tonic::server::UnaryService<super::QuestionStoreServiceGetGlobalRequest>
                        for GetGlobalSvc<T>
                    {
                        type Response = super::QuestionStoreServiceGetGlobalResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::QuestionStoreServiceGetGlobalRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_global(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = GetGlobalSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/enem.metadata.v1.QuestionStoreService/Save" => {
                    #[allow(non_camel_case_types)]
                    struct SaveSvc<T: QuestionStoreService>(pub Arc<T>);
                    impl<T: QuestionStoreService>
                        tonic::server::UnaryService<super::QuestionStoreServiceSaveRequest>
                        for SaveSvc<T>
                    {
                        type Response = super::QuestionStoreServiceSaveResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::QuestionStoreServiceSaveRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).save(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = SaveSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/enem.metadata.v1.QuestionStoreService/Delete" => {
                    #[allow(non_camel_case_types)]
                    struct DeleteSvc<T: QuestionStoreService>(pub Arc<T>);
                    impl<T: QuestionStoreService>
                        tonic::server::UnaryService<super::QuestionStoreServiceDeleteRequest>
                        for DeleteSvc<T>
                    {
                        type Response = super::QuestionStoreServiceDeleteResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::QuestionStoreServiceDeleteRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).delete(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = DeleteSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .header("content-type", "application/grpc")
                        .body(empty_body())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: QuestionStoreService> Clone for QuestionStoreServiceServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self {
                inner,
                accept_compression_encodings: self.accept_compression_encodings,
                send_compression_encodings: self.send_compression_encodings,
            }
        }
    }
    impl<T: QuestionStoreService> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: QuestionStoreService> tonic::transport::NamedService for QuestionStoreServiceServer<T> {
        const NAME: &'static str = "enem.metadata.v1.QuestionStoreService";
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CompetenceStoreServiceGetGlobalRequest {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CompetenceStoreServiceGetGlobalResponse {
    #[prost(message, optional, tag = "1")]
    pub competence: ::core::option::Option<Competence>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CompetenceStoreServiceGetLocalRequest {
    #[prost(enumeration = "Subject", tag = "1")]
    pub subject: i32,
    #[prost(uint32, tag = "2")]
    pub local_id: u32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CompetenceStoreServiceGetLocalResponse {
    #[prost(message, optional, tag = "1")]
    pub competence: ::core::option::Option<Competence>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CompetenceStoreServiceCreateRequest {
    #[prost(enumeration = "Subject", tag = "1")]
    pub subject: i32,
    #[prost(string, optional, tag = "2")]
    pub description: ::core::option::Option<::prost::alloc::string::String>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CompetenceStoreServiceCreateResponse {
    #[prost(message, optional, tag = "1")]
    pub competence: ::core::option::Option<Competence>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CompetenceStoreServiceUpdateRequest {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
    #[prost(string, optional, tag = "2")]
    pub description: ::core::option::Option<::prost::alloc::string::String>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CompetenceStoreServiceUpdateResponse {
    #[prost(message, optional, tag = "1")]
    pub competence: ::core::option::Option<Competence>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CompetenceStoreServiceDeleteRequest {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CompetenceStoreServiceDeleteResponse {
    #[prost(message, optional, tag = "1")]
    pub competence: ::core::option::Option<Competence>,
}
#[doc = r" Generated client implementations."]
pub mod competence_store_service_client {
    #![allow(unused_variables, dead_code, missing_docs, clippy::let_unit_value)]
    use tonic::codegen::*;
    #[derive(Debug, Clone)]
    pub struct CompetenceStoreServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl CompetenceStoreServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> CompetenceStoreServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + Send + Sync + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as Body>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor<F>(
            inner: T,
            interceptor: F,
        ) -> CompetenceStoreServiceClient<InterceptedService<T, F>>
        where
            F: tonic::service::Interceptor,
            T: tonic::codegen::Service<
                http::Request<tonic::body::BoxBody>,
                Response = http::Response<
                    <T as tonic::client::GrpcService<tonic::body::BoxBody>>::ResponseBody,
                >,
            >,
            <T as tonic::codegen::Service<http::Request<tonic::body::BoxBody>>>::Error:
                Into<StdError> + Send + Sync,
        {
            CompetenceStoreServiceClient::new(InterceptedService::new(inner, interceptor))
        }
        #[doc = r" Compress requests with `gzip`."]
        #[doc = r""]
        #[doc = r" This requires the server to support it otherwise it might respond with an"]
        #[doc = r" error."]
        pub fn send_gzip(mut self) -> Self {
            self.inner = self.inner.send_gzip();
            self
        }
        #[doc = r" Enable decompressing responses with `gzip`."]
        pub fn accept_gzip(mut self) -> Self {
            self.inner = self.inner.accept_gzip();
            self
        }
        pub async fn get_global(
            &mut self,
            request: impl tonic::IntoRequest<super::CompetenceStoreServiceGetGlobalRequest>,
        ) -> Result<tonic::Response<super::CompetenceStoreServiceGetGlobalResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/enem.metadata.v1.CompetenceStoreService/GetGlobal",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_local(
            &mut self,
            request: impl tonic::IntoRequest<super::CompetenceStoreServiceGetLocalRequest>,
        ) -> Result<tonic::Response<super::CompetenceStoreServiceGetLocalResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/enem.metadata.v1.CompetenceStoreService/GetLocal",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn create(
            &mut self,
            request: impl tonic::IntoRequest<super::CompetenceStoreServiceCreateRequest>,
        ) -> Result<tonic::Response<super::CompetenceStoreServiceCreateResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/enem.metadata.v1.CompetenceStoreService/Create",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn update(
            &mut self,
            request: impl tonic::IntoRequest<super::CompetenceStoreServiceUpdateRequest>,
        ) -> Result<tonic::Response<super::CompetenceStoreServiceUpdateResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/enem.metadata.v1.CompetenceStoreService/Update",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn delete(
            &mut self,
            request: impl tonic::IntoRequest<super::CompetenceStoreServiceDeleteRequest>,
        ) -> Result<tonic::Response<super::CompetenceStoreServiceDeleteResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/enem.metadata.v1.CompetenceStoreService/Delete",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod competence_store_service_server {
    #![allow(unused_variables, dead_code, missing_docs, clippy::let_unit_value)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with CompetenceStoreServiceServer."]
    #[async_trait]
    pub trait CompetenceStoreService: Send + Sync + 'static {
        async fn get_global(
            &self,
            request: tonic::Request<super::CompetenceStoreServiceGetGlobalRequest>,
        ) -> Result<tonic::Response<super::CompetenceStoreServiceGetGlobalResponse>, tonic::Status>;
        async fn get_local(
            &self,
            request: tonic::Request<super::CompetenceStoreServiceGetLocalRequest>,
        ) -> Result<tonic::Response<super::CompetenceStoreServiceGetLocalResponse>, tonic::Status>;
        async fn create(
            &self,
            request: tonic::Request<super::CompetenceStoreServiceCreateRequest>,
        ) -> Result<tonic::Response<super::CompetenceStoreServiceCreateResponse>, tonic::Status>;
        async fn update(
            &self,
            request: tonic::Request<super::CompetenceStoreServiceUpdateRequest>,
        ) -> Result<tonic::Response<super::CompetenceStoreServiceUpdateResponse>, tonic::Status>;
        async fn delete(
            &self,
            request: tonic::Request<super::CompetenceStoreServiceDeleteRequest>,
        ) -> Result<tonic::Response<super::CompetenceStoreServiceDeleteResponse>, tonic::Status>;
    }
    #[derive(Debug)]
    pub struct CompetenceStoreServiceServer<T: CompetenceStoreService> {
        inner: _Inner<T>,
        accept_compression_encodings: (),
        send_compression_encodings: (),
    }
    struct _Inner<T>(Arc<T>);
    impl<T: CompetenceStoreService> CompetenceStoreServiceServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner);
            Self {
                inner,
                accept_compression_encodings: Default::default(),
                send_compression_encodings: Default::default(),
            }
        }
        pub fn with_interceptor<F>(inner: T, interceptor: F) -> InterceptedService<Self, F>
        where
            F: tonic::service::Interceptor,
        {
            InterceptedService::new(Self::new(inner), interceptor)
        }
    }
    impl<T, B> tonic::codegen::Service<http::Request<B>> for CompetenceStoreServiceServer<T>
    where
        T: CompetenceStoreService,
        B: Body + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/enem.metadata.v1.CompetenceStoreService/GetGlobal" => {
                    #[allow(non_camel_case_types)]
                    struct GetGlobalSvc<T: CompetenceStoreService>(pub Arc<T>);
                    impl<T: CompetenceStoreService>
                        tonic::server::UnaryService<super::CompetenceStoreServiceGetGlobalRequest>
                        for GetGlobalSvc<T>
                    {
                        type Response = super::CompetenceStoreServiceGetGlobalResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::CompetenceStoreServiceGetGlobalRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_global(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = GetGlobalSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/enem.metadata.v1.CompetenceStoreService/GetLocal" => {
                    #[allow(non_camel_case_types)]
                    struct GetLocalSvc<T: CompetenceStoreService>(pub Arc<T>);
                    impl<T: CompetenceStoreService>
                        tonic::server::UnaryService<super::CompetenceStoreServiceGetLocalRequest>
                        for GetLocalSvc<T>
                    {
                        type Response = super::CompetenceStoreServiceGetLocalResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::CompetenceStoreServiceGetLocalRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_local(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = GetLocalSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/enem.metadata.v1.CompetenceStoreService/Create" => {
                    #[allow(non_camel_case_types)]
                    struct CreateSvc<T: CompetenceStoreService>(pub Arc<T>);
                    impl<T: CompetenceStoreService>
                        tonic::server::UnaryService<super::CompetenceStoreServiceCreateRequest>
                        for CreateSvc<T>
                    {
                        type Response = super::CompetenceStoreServiceCreateResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::CompetenceStoreServiceCreateRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).create(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = CreateSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/enem.metadata.v1.CompetenceStoreService/Update" => {
                    #[allow(non_camel_case_types)]
                    struct UpdateSvc<T: CompetenceStoreService>(pub Arc<T>);
                    impl<T: CompetenceStoreService>
                        tonic::server::UnaryService<super::CompetenceStoreServiceUpdateRequest>
                        for UpdateSvc<T>
                    {
                        type Response = super::CompetenceStoreServiceUpdateResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::CompetenceStoreServiceUpdateRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).update(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = UpdateSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/enem.metadata.v1.CompetenceStoreService/Delete" => {
                    #[allow(non_camel_case_types)]
                    struct DeleteSvc<T: CompetenceStoreService>(pub Arc<T>);
                    impl<T: CompetenceStoreService>
                        tonic::server::UnaryService<super::CompetenceStoreServiceDeleteRequest>
                        for DeleteSvc<T>
                    {
                        type Response = super::CompetenceStoreServiceDeleteResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::CompetenceStoreServiceDeleteRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).delete(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = DeleteSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .header("content-type", "application/grpc")
                        .body(empty_body())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: CompetenceStoreService> Clone for CompetenceStoreServiceServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self {
                inner,
                accept_compression_encodings: self.accept_compression_encodings,
                send_compression_encodings: self.send_compression_encodings,
            }
        }
    }
    impl<T: CompetenceStoreService> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: CompetenceStoreService> tonic::transport::NamedService for CompetenceStoreServiceServer<T> {
        const NAME: &'static str = "enem.metadata.v1.CompetenceStoreService";
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ExamBookStoreServiceGetGlobalRequest {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ExamBookStoreServiceGetGlobalResponse {
    #[prost(message, optional, tag = "1")]
    pub exam_book: ::core::option::Option<ExamBook>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ExamBookStoreServiceGetLocalRequest {
    #[prost(uint32, tag = "1")]
    pub year: u32,
    #[prost(uint32, tag = "2")]
    pub local_id: u32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ExamBookStoreServiceGetLocalResponse {
    #[prost(message, optional, tag = "1")]
    pub exam_book: ::core::option::Option<ExamBook>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ExamBookStoreServiceCreateRequest {
    #[prost(uint32, tag = "2")]
    pub year: u32,
    /// The INEP ID for this particular ExamBook. These are unique within the same year.
    #[prost(uint32, tag = "3")]
    pub local_id: u32,
    /// The color of the ExamBook, used to identify the ordering of the questions by the participants.
    /// Here the value is normalized to full lower case.
    #[prost(string, tag = "4")]
    pub color: ::prost::alloc::string::String,
    #[prost(enumeration = "Subject", tag = "5")]
    pub subject: i32,
    /// An ExamBook of SUBJECT_LC must have 50 questions in this order:
    ///  - English specific questions, numbered 1 - 5
    ///  - Spanish specific questions, numbered 1 - 5
    ///  - Native language questions, numbered 6 - 45
    ///
    /// ExamBooks of any other subject must have exactly 45 questions.
    #[prost(message, repeated, tag = "6")]
    pub questions: ::prost::alloc::vec::Vec<Question>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ExamBookStoreServiceCreateResponse {
    #[prost(message, optional, tag = "1")]
    pub exam_book: ::core::option::Option<ExamBook>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ExamBookStoreServiceUpdateRequest {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
    #[prost(enumeration = "Subject", optional, tag = "5")]
    pub subject: ::core::option::Option<i32>,
    /// An ExamBook of SUBJECT_LC must have 50 questions in this order:
    ///  - English specific questions, numbered 1 - 5
    ///  - Spanish specific questions, numbered 1 - 5
    ///  - Native language questions, numbered 6 - 45
    ///
    /// ExamBooks of any other subject must have exactly 45 questions.
    ///
    /// Passing an empty list will keep the current questions.
    #[prost(message, repeated, tag = "6")]
    pub questions: ::prost::alloc::vec::Vec<Question>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ExamBookStoreServiceUpdateResponse {
    #[prost(message, optional, tag = "1")]
    pub exam_book: ::core::option::Option<ExamBook>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ExamBookStoreServiceDeleteRequest {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ExamBookStoreServiceDeleteResponse {
    #[prost(message, optional, tag = "1")]
    pub exam_book: ::core::option::Option<ExamBook>,
}
#[doc = r" Generated client implementations."]
pub mod exam_book_store_service_client {
    #![allow(unused_variables, dead_code, missing_docs, clippy::let_unit_value)]
    use tonic::codegen::*;
    #[derive(Debug, Clone)]
    pub struct ExamBookStoreServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl ExamBookStoreServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> ExamBookStoreServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + Send + Sync + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as Body>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor<F>(
            inner: T,
            interceptor: F,
        ) -> ExamBookStoreServiceClient<InterceptedService<T, F>>
        where
            F: tonic::service::Interceptor,
            T: tonic::codegen::Service<
                http::Request<tonic::body::BoxBody>,
                Response = http::Response<
                    <T as tonic::client::GrpcService<tonic::body::BoxBody>>::ResponseBody,
                >,
            >,
            <T as tonic::codegen::Service<http::Request<tonic::body::BoxBody>>>::Error:
                Into<StdError> + Send + Sync,
        {
            ExamBookStoreServiceClient::new(InterceptedService::new(inner, interceptor))
        }
        #[doc = r" Compress requests with `gzip`."]
        #[doc = r""]
        #[doc = r" This requires the server to support it otherwise it might respond with an"]
        #[doc = r" error."]
        pub fn send_gzip(mut self) -> Self {
            self.inner = self.inner.send_gzip();
            self
        }
        #[doc = r" Enable decompressing responses with `gzip`."]
        pub fn accept_gzip(mut self) -> Self {
            self.inner = self.inner.accept_gzip();
            self
        }
        pub async fn get_global(
            &mut self,
            request: impl tonic::IntoRequest<super::ExamBookStoreServiceGetGlobalRequest>,
        ) -> Result<tonic::Response<super::ExamBookStoreServiceGetGlobalResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/enem.metadata.v1.ExamBookStoreService/GetGlobal",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_local(
            &mut self,
            request: impl tonic::IntoRequest<super::ExamBookStoreServiceGetLocalRequest>,
        ) -> Result<tonic::Response<super::ExamBookStoreServiceGetLocalResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/enem.metadata.v1.ExamBookStoreService/GetLocal",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn create(
            &mut self,
            request: impl tonic::IntoRequest<super::ExamBookStoreServiceCreateRequest>,
        ) -> Result<tonic::Response<super::ExamBookStoreServiceCreateResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/enem.metadata.v1.ExamBookStoreService/Create",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn update(
            &mut self,
            request: impl tonic::IntoRequest<super::ExamBookStoreServiceUpdateRequest>,
        ) -> Result<tonic::Response<super::ExamBookStoreServiceUpdateResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/enem.metadata.v1.ExamBookStoreService/Update",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn delete(
            &mut self,
            request: impl tonic::IntoRequest<super::ExamBookStoreServiceDeleteRequest>,
        ) -> Result<tonic::Response<super::ExamBookStoreServiceDeleteResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/enem.metadata.v1.ExamBookStoreService/Delete",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod exam_book_store_service_server {
    #![allow(unused_variables, dead_code, missing_docs, clippy::let_unit_value)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with ExamBookStoreServiceServer."]
    #[async_trait]
    pub trait ExamBookStoreService: Send + Sync + 'static {
        async fn get_global(
            &self,
            request: tonic::Request<super::ExamBookStoreServiceGetGlobalRequest>,
        ) -> Result<tonic::Response<super::ExamBookStoreServiceGetGlobalResponse>, tonic::Status>;
        async fn get_local(
            &self,
            request: tonic::Request<super::ExamBookStoreServiceGetLocalRequest>,
        ) -> Result<tonic::Response<super::ExamBookStoreServiceGetLocalResponse>, tonic::Status>;
        async fn create(
            &self,
            request: tonic::Request<super::ExamBookStoreServiceCreateRequest>,
        ) -> Result<tonic::Response<super::ExamBookStoreServiceCreateResponse>, tonic::Status>;
        async fn update(
            &self,
            request: tonic::Request<super::ExamBookStoreServiceUpdateRequest>,
        ) -> Result<tonic::Response<super::ExamBookStoreServiceUpdateResponse>, tonic::Status>;
        async fn delete(
            &self,
            request: tonic::Request<super::ExamBookStoreServiceDeleteRequest>,
        ) -> Result<tonic::Response<super::ExamBookStoreServiceDeleteResponse>, tonic::Status>;
    }
    #[derive(Debug)]
    pub struct ExamBookStoreServiceServer<T: ExamBookStoreService> {
        inner: _Inner<T>,
        accept_compression_encodings: (),
        send_compression_encodings: (),
    }
    struct _Inner<T>(Arc<T>);
    impl<T: ExamBookStoreService> ExamBookStoreServiceServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner);
            Self {
                inner,
                accept_compression_encodings: Default::default(),
                send_compression_encodings: Default::default(),
            }
        }
        pub fn with_interceptor<F>(inner: T, interceptor: F) -> InterceptedService<Self, F>
        where
            F: tonic::service::Interceptor,
        {
            InterceptedService::new(Self::new(inner), interceptor)
        }
    }
    impl<T, B> tonic::codegen::Service<http::Request<B>> for ExamBookStoreServiceServer<T>
    where
        T: ExamBookStoreService,
        B: Body + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/enem.metadata.v1.ExamBookStoreService/GetGlobal" => {
                    #[allow(non_camel_case_types)]
                    struct GetGlobalSvc<T: ExamBookStoreService>(pub Arc<T>);
                    impl<T: ExamBookStoreService>
                        tonic::server::UnaryService<super::ExamBookStoreServiceGetGlobalRequest>
                        for GetGlobalSvc<T>
                    {
                        type Response = super::ExamBookStoreServiceGetGlobalResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ExamBookStoreServiceGetGlobalRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_global(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = GetGlobalSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/enem.metadata.v1.ExamBookStoreService/GetLocal" => {
                    #[allow(non_camel_case_types)]
                    struct GetLocalSvc<T: ExamBookStoreService>(pub Arc<T>);
                    impl<T: ExamBookStoreService>
                        tonic::server::UnaryService<super::ExamBookStoreServiceGetLocalRequest>
                        for GetLocalSvc<T>
                    {
                        type Response = super::ExamBookStoreServiceGetLocalResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ExamBookStoreServiceGetLocalRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_local(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = GetLocalSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/enem.metadata.v1.ExamBookStoreService/Create" => {
                    #[allow(non_camel_case_types)]
                    struct CreateSvc<T: ExamBookStoreService>(pub Arc<T>);
                    impl<T: ExamBookStoreService>
                        tonic::server::UnaryService<super::ExamBookStoreServiceCreateRequest>
                        for CreateSvc<T>
                    {
                        type Response = super::ExamBookStoreServiceCreateResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ExamBookStoreServiceCreateRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).create(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = CreateSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/enem.metadata.v1.ExamBookStoreService/Update" => {
                    #[allow(non_camel_case_types)]
                    struct UpdateSvc<T: ExamBookStoreService>(pub Arc<T>);
                    impl<T: ExamBookStoreService>
                        tonic::server::UnaryService<super::ExamBookStoreServiceUpdateRequest>
                        for UpdateSvc<T>
                    {
                        type Response = super::ExamBookStoreServiceUpdateResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ExamBookStoreServiceUpdateRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).update(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = UpdateSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/enem.metadata.v1.ExamBookStoreService/Delete" => {
                    #[allow(non_camel_case_types)]
                    struct DeleteSvc<T: ExamBookStoreService>(pub Arc<T>);
                    impl<T: ExamBookStoreService>
                        tonic::server::UnaryService<super::ExamBookStoreServiceDeleteRequest>
                        for DeleteSvc<T>
                    {
                        type Response = super::ExamBookStoreServiceDeleteResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::ExamBookStoreServiceDeleteRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).delete(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = DeleteSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .header("content-type", "application/grpc")
                        .body(empty_body())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: ExamBookStoreService> Clone for ExamBookStoreServiceServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self {
                inner,
                accept_compression_encodings: self.accept_compression_encodings,
                send_compression_encodings: self.send_compression_encodings,
            }
        }
    }
    impl<T: ExamBookStoreService> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: ExamBookStoreService> tonic::transport::NamedService for ExamBookStoreServiceServer<T> {
        const NAME: &'static str = "enem.metadata.v1.ExamBookStoreService";
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SkillStoreServiceGetGlobalRequest {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SkillStoreServiceGetGlobalResponse {
    #[prost(message, optional, tag = "1")]
    pub skill: ::core::option::Option<Skill>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SkillStoreServiceGetLocalRequest {
    #[prost(uint32, tag = "1")]
    pub competence_id: u32,
    #[prost(uint32, tag = "2")]
    pub local_id: u32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SkillStoreServiceGetLocalResponse {
    #[prost(message, optional, tag = "1")]
    pub skill: ::core::option::Option<Skill>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SkillStoreServiceCreateRequest {
    #[prost(uint32, tag = "1")]
    pub competence_id: u32,
    #[prost(string, optional, tag = "2")]
    pub description: ::core::option::Option<::prost::alloc::string::String>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SkillStoreServiceCreateResponse {
    #[prost(message, optional, tag = "1")]
    pub skill: ::core::option::Option<Skill>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SkillStoreServiceUpdateRequest {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
    #[prost(string, optional, tag = "2")]
    pub description: ::core::option::Option<::prost::alloc::string::String>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SkillStoreServiceUpdateResponse {
    #[prost(message, optional, tag = "1")]
    pub skill: ::core::option::Option<Skill>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SkillStoreServiceDeleteRequest {
    #[prost(uint32, tag = "1")]
    pub global_id: u32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SkillStoreServiceDeleteResponse {
    #[prost(message, optional, tag = "1")]
    pub skill: ::core::option::Option<Skill>,
}
#[doc = r" Generated client implementations."]
pub mod skill_store_service_client {
    #![allow(unused_variables, dead_code, missing_docs, clippy::let_unit_value)]
    use tonic::codegen::*;
    #[derive(Debug, Clone)]
    pub struct SkillStoreServiceClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl SkillStoreServiceClient<tonic::transport::Channel> {
        #[doc = r" Attempt to create a new client by connecting to a given endpoint."]
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: std::convert::TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> SkillStoreServiceClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::ResponseBody: Body + Send + Sync + 'static,
        T::Error: Into<StdError>,
        <T::ResponseBody as Body>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_interceptor<F>(
            inner: T,
            interceptor: F,
        ) -> SkillStoreServiceClient<InterceptedService<T, F>>
        where
            F: tonic::service::Interceptor,
            T: tonic::codegen::Service<
                http::Request<tonic::body::BoxBody>,
                Response = http::Response<
                    <T as tonic::client::GrpcService<tonic::body::BoxBody>>::ResponseBody,
                >,
            >,
            <T as tonic::codegen::Service<http::Request<tonic::body::BoxBody>>>::Error:
                Into<StdError> + Send + Sync,
        {
            SkillStoreServiceClient::new(InterceptedService::new(inner, interceptor))
        }
        #[doc = r" Compress requests with `gzip`."]
        #[doc = r""]
        #[doc = r" This requires the server to support it otherwise it might respond with an"]
        #[doc = r" error."]
        pub fn send_gzip(mut self) -> Self {
            self.inner = self.inner.send_gzip();
            self
        }
        #[doc = r" Enable decompressing responses with `gzip`."]
        pub fn accept_gzip(mut self) -> Self {
            self.inner = self.inner.accept_gzip();
            self
        }
        pub async fn get_global(
            &mut self,
            request: impl tonic::IntoRequest<super::SkillStoreServiceGetGlobalRequest>,
        ) -> Result<tonic::Response<super::SkillStoreServiceGetGlobalResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/enem.metadata.v1.SkillStoreService/GetGlobal",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn get_local(
            &mut self,
            request: impl tonic::IntoRequest<super::SkillStoreServiceGetLocalRequest>,
        ) -> Result<tonic::Response<super::SkillStoreServiceGetLocalResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/enem.metadata.v1.SkillStoreService/GetLocal",
            );
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn create(
            &mut self,
            request: impl tonic::IntoRequest<super::SkillStoreServiceCreateRequest>,
        ) -> Result<tonic::Response<super::SkillStoreServiceCreateResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/enem.metadata.v1.SkillStoreService/Create");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn update(
            &mut self,
            request: impl tonic::IntoRequest<super::SkillStoreServiceUpdateRequest>,
        ) -> Result<tonic::Response<super::SkillStoreServiceUpdateResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/enem.metadata.v1.SkillStoreService/Update");
            self.inner.unary(request.into_request(), path, codec).await
        }
        pub async fn delete(
            &mut self,
            request: impl tonic::IntoRequest<super::SkillStoreServiceDeleteRequest>,
        ) -> Result<tonic::Response<super::SkillStoreServiceDeleteResponse>, tonic::Status>
        {
            self.inner.ready().await.map_err(|e| {
                tonic::Status::new(
                    tonic::Code::Unknown,
                    format!("Service was not ready: {}", e.into()),
                )
            })?;
            let codec = tonic::codec::ProstCodec::default();
            let path =
                http::uri::PathAndQuery::from_static("/enem.metadata.v1.SkillStoreService/Delete");
            self.inner.unary(request.into_request(), path, codec).await
        }
    }
}
#[doc = r" Generated server implementations."]
pub mod skill_store_service_server {
    #![allow(unused_variables, dead_code, missing_docs, clippy::let_unit_value)]
    use tonic::codegen::*;
    #[doc = "Generated trait containing gRPC methods that should be implemented for use with SkillStoreServiceServer."]
    #[async_trait]
    pub trait SkillStoreService: Send + Sync + 'static {
        async fn get_global(
            &self,
            request: tonic::Request<super::SkillStoreServiceGetGlobalRequest>,
        ) -> Result<tonic::Response<super::SkillStoreServiceGetGlobalResponse>, tonic::Status>;
        async fn get_local(
            &self,
            request: tonic::Request<super::SkillStoreServiceGetLocalRequest>,
        ) -> Result<tonic::Response<super::SkillStoreServiceGetLocalResponse>, tonic::Status>;
        async fn create(
            &self,
            request: tonic::Request<super::SkillStoreServiceCreateRequest>,
        ) -> Result<tonic::Response<super::SkillStoreServiceCreateResponse>, tonic::Status>;
        async fn update(
            &self,
            request: tonic::Request<super::SkillStoreServiceUpdateRequest>,
        ) -> Result<tonic::Response<super::SkillStoreServiceUpdateResponse>, tonic::Status>;
        async fn delete(
            &self,
            request: tonic::Request<super::SkillStoreServiceDeleteRequest>,
        ) -> Result<tonic::Response<super::SkillStoreServiceDeleteResponse>, tonic::Status>;
    }
    #[derive(Debug)]
    pub struct SkillStoreServiceServer<T: SkillStoreService> {
        inner: _Inner<T>,
        accept_compression_encodings: (),
        send_compression_encodings: (),
    }
    struct _Inner<T>(Arc<T>);
    impl<T: SkillStoreService> SkillStoreServiceServer<T> {
        pub fn new(inner: T) -> Self {
            let inner = Arc::new(inner);
            let inner = _Inner(inner);
            Self {
                inner,
                accept_compression_encodings: Default::default(),
                send_compression_encodings: Default::default(),
            }
        }
        pub fn with_interceptor<F>(inner: T, interceptor: F) -> InterceptedService<Self, F>
        where
            F: tonic::service::Interceptor,
        {
            InterceptedService::new(Self::new(inner), interceptor)
        }
    }
    impl<T, B> tonic::codegen::Service<http::Request<B>> for SkillStoreServiceServer<T>
    where
        T: SkillStoreService,
        B: Body + Send + Sync + 'static,
        B::Error: Into<StdError> + Send + 'static,
    {
        type Response = http::Response<tonic::body::BoxBody>;
        type Error = Never;
        type Future = BoxFuture<Self::Response, Self::Error>;
        fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
        fn call(&mut self, req: http::Request<B>) -> Self::Future {
            let inner = self.inner.clone();
            match req.uri().path() {
                "/enem.metadata.v1.SkillStoreService/GetGlobal" => {
                    #[allow(non_camel_case_types)]
                    struct GetGlobalSvc<T: SkillStoreService>(pub Arc<T>);
                    impl<T: SkillStoreService>
                        tonic::server::UnaryService<super::SkillStoreServiceGetGlobalRequest>
                        for GetGlobalSvc<T>
                    {
                        type Response = super::SkillStoreServiceGetGlobalResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::SkillStoreServiceGetGlobalRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_global(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = GetGlobalSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/enem.metadata.v1.SkillStoreService/GetLocal" => {
                    #[allow(non_camel_case_types)]
                    struct GetLocalSvc<T: SkillStoreService>(pub Arc<T>);
                    impl<T: SkillStoreService>
                        tonic::server::UnaryService<super::SkillStoreServiceGetLocalRequest>
                        for GetLocalSvc<T>
                    {
                        type Response = super::SkillStoreServiceGetLocalResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::SkillStoreServiceGetLocalRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).get_local(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = GetLocalSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/enem.metadata.v1.SkillStoreService/Create" => {
                    #[allow(non_camel_case_types)]
                    struct CreateSvc<T: SkillStoreService>(pub Arc<T>);
                    impl<T: SkillStoreService>
                        tonic::server::UnaryService<super::SkillStoreServiceCreateRequest>
                        for CreateSvc<T>
                    {
                        type Response = super::SkillStoreServiceCreateResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::SkillStoreServiceCreateRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).create(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = CreateSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/enem.metadata.v1.SkillStoreService/Update" => {
                    #[allow(non_camel_case_types)]
                    struct UpdateSvc<T: SkillStoreService>(pub Arc<T>);
                    impl<T: SkillStoreService>
                        tonic::server::UnaryService<super::SkillStoreServiceUpdateRequest>
                        for UpdateSvc<T>
                    {
                        type Response = super::SkillStoreServiceUpdateResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::SkillStoreServiceUpdateRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).update(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = UpdateSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                "/enem.metadata.v1.SkillStoreService/Delete" => {
                    #[allow(non_camel_case_types)]
                    struct DeleteSvc<T: SkillStoreService>(pub Arc<T>);
                    impl<T: SkillStoreService>
                        tonic::server::UnaryService<super::SkillStoreServiceDeleteRequest>
                        for DeleteSvc<T>
                    {
                        type Response = super::SkillStoreServiceDeleteResponse;
                        type Future = BoxFuture<tonic::Response<Self::Response>, tonic::Status>;
                        fn call(
                            &mut self,
                            request: tonic::Request<super::SkillStoreServiceDeleteRequest>,
                        ) -> Self::Future {
                            let inner = self.0.clone();
                            let fut = async move { (*inner).delete(request).await };
                            Box::pin(fut)
                        }
                    }
                    let accept_compression_encodings = self.accept_compression_encodings;
                    let send_compression_encodings = self.send_compression_encodings;
                    let inner = self.inner.clone();
                    let fut = async move {
                        let inner = inner.0;
                        let method = DeleteSvc(inner);
                        let codec = tonic::codec::ProstCodec::default();
                        let mut grpc = tonic::server::Grpc::new(codec).apply_compression_config(
                            accept_compression_encodings,
                            send_compression_encodings,
                        );
                        let res = grpc.unary(method, req).await;
                        Ok(res)
                    };
                    Box::pin(fut)
                }
                _ => Box::pin(async move {
                    Ok(http::Response::builder()
                        .status(200)
                        .header("grpc-status", "12")
                        .header("content-type", "application/grpc")
                        .body(empty_body())
                        .unwrap())
                }),
            }
        }
    }
    impl<T: SkillStoreService> Clone for SkillStoreServiceServer<T> {
        fn clone(&self) -> Self {
            let inner = self.inner.clone();
            Self {
                inner,
                accept_compression_encodings: self.accept_compression_encodings,
                send_compression_encodings: self.send_compression_encodings,
            }
        }
    }
    impl<T: SkillStoreService> Clone for _Inner<T> {
        fn clone(&self) -> Self {
            Self(self.0.clone())
        }
    }
    impl<T: std::fmt::Debug> std::fmt::Debug for _Inner<T> {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self.0)
        }
    }
    impl<T: SkillStoreService> tonic::transport::NamedService for SkillStoreServiceServer<T> {
        const NAME: &'static str = "enem.metadata.v1.SkillStoreService";
    }
}
